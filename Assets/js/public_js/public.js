/**
 * Created by leonid on 01.05.15.
 */
(function(){
  'use strict';
    var Style;
    Style = {
      pageHref: window.location.href,
      url: document.URL,

      style: function (footerClass) {
        if (this.pageHref === 'http://papens.com/about.php') {
          $('footer').addClass(footerClass);
        }
      },

      colorLink: function (color) {
        for (var lnk = document.links, j = 0; j < lnk.length; j++) {
          if (lnk [j].href === this.url) {
            lnk [j].style.color = color;
          }
        }
      },

      error: function(input){
        $(input).css({
          'border-bottom': '2px groove red'
        });
      },

      success: function(input){
        $(input).css({
          'border-bottom': '2px groove #20AE0F'
        });
        return this;
      },

      footerInfo: function(el, opts){
        opts = opts || {};
        $(el).children().addClass(opts);
         return this;
      }
    };

    var MailManager;

    MailManager = {
      // emailPattern: '/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/',

      errors      : [],

      validation: function (input) {
        var errors, testEmail, email;
        errors  = this.errors;

        if ($(input).val() === '') {
          errors.push('input ' + $(input).attr('name') + ' can not be empty!');
          Style.error(input);
        }
        else {
          Style.success(input);
        }
        if ($(input).attr('type') === 'email') {
          email = this.setEmailToLower(input);
          testEmail = this.emailPattern.test(email);
          if (!testEmail) {
            Style.error(input);
            errors.push('please, fill the correct email address.');
          }
        }
        return this;
      },

      setEmailToLower: function(input){
        this.input = input;
        this.input = ($(input).val()).toLowerCase();
        return this.input;
      },

      checkError: function(){
        var setError, i;
        setError = this.validation().errors;

        if (setError.length>0) {
          for (i = 0; i < setError.length; i++) {
            $.notify(setError[i]);
          }
        }
        this.errors = [];
      },

      sendMail: function(data){
       var console = window.console;
        $.ajax({
          type:"POST",
          url: "../../public_controllers/ajaxController.php",
          data: data
        })
          .done(function(data){
            console.log(data);
          })
          .fail(function(data){
            console.log(data);
          });
      }
    };

    Style.colorLink('#00BFFF');
    Style.style('about-footer');
    
    Style.footerInfo('.footer-left-info','text-muted small');
    Style.footerInfo('.footer-right-info', 'text-muted small text-right');

    if (Style.pageHref === 'http://papens.com/about.php') {
      $('button').on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var i, form, data;
        form = e.delegateTarget.form;
        data = $(form).serializeArray();
        window.console.log(data);
        for (i=0; i<form.length; i++) {
          if ($(form[i]).hasClass('form-group')) {
            MailManager.validation(form[i]);
          }
        }
        if (MailManager.validation().errors.length === 0) {
          //return;
          MailManager.sendMail(data);
        }
        MailManager.checkError();
      });
    }
})();
