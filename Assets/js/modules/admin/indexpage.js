/**
 * Created by leonid on 28.05.15.
 */
/*manage index.php page*/
(function(){
  'use strict';
 var console = window.console;
if (window.location.pathname === "/admin/index.php") {
  $("#addTags").on('click', function (e) {
    e.preventDefault();
    var data = $('#addTagForm').serializeArray();

    $.ajax({
      type: "POST",
      url: "../admin/controller/ajaxController.php",
      data: data
    })
      .done(function (data) {
        data = $.parseJSON(data);
        $.notify('Tag ' + data.data.msg + ' was added', data.data.status);
        window.Common.updateUI(1500);
      })
      .fail(function(data){
        data = $.parseJSON(data);
        $.notify('Error during adding a tag: ' + data.data.msg + '!!!', data.data.status);
      });
    $('#exampleModal').modal('hide');
  });

  $('.update').on('click', function (e) {
    e.preventDefault();
    var tagId, tag, modal;
    tagId = this.dataset.update;
    tag = $($(this).parent().parent().find('td')[0]).text();

    $('#updateTagModal').on('show.bs.modal', function () {
      modal = $(this);
      modal.find('[name="updatedTag"]').val(tag);
      modal.find('[name="tagUpdateControl"]').val(tagId);
    });
  });

  $('#updateTag').on('click', function(){
    var data;
    data = $('#UpdateTagForm').serializeArray();
    $.ajax({
      type: "POST",
      url: '../admin/controller/ajaxController.php',
      data: data
    })
      .done(function(data){
        data = $.parseJSON(data);
        $('#updateTagModal').modal('hide');
        $.notify('tag "'+ data.success + '" updated successfully.', 'success');
        window.Common.updateUI(1500);
      })
      .fail(function (data) {
        console.log(data);
      });
  });

  $('#confirmModal').on('show.bs.modal', function (e) {
    var data;
    data = e.relatedTarget.dataset.remove;

    $('#deleteItem').on('click', function(){
      $.ajax({
        type: "POST",
        url: "../admin/controller/ajaxController.php",
        data: {
          'deleteTag':data
        }
      })
        .done(function(data){
          data = $.parseJSON(data);
          $('#confirmModal').modal('hide');
          $.notify(data.data.success, 'success');
          window.Common.updateUI(1500);
        })
        .fail(function (data) {
          data = $.parseJSON(data);
          $.notify(data.data.error, 'error');
        });
    });
  });

  $('.delete-user').on('click', function (e) {
    e.preventDefault();
    var id = this.dataset.userDelete;
    var row = $(this).parent().parent();

    var confirmDelete =  window.confirm('delete user #' + id);
    if (confirmDelete) {
      $.ajax({
        type: "POST",
        url: '../admin/controller/ajaxController.php',
        data: {'deleteUser':id}
      })
        .done(function (data) {
          data = $.parseJSON(data);
          $.notify(data.success, 'success');
          $(row).fadeOut(500, function () {
            $(row).remove();
          });
        })
        .fail(function (data) {
          data = $.parseJSON(data);
          $.notify(data.error, 'error');
        });
    }
  });


  /*manage users table */
  $("#addUsers").on('click', function (e) {
    e.preventDefault();
    var data = $("#addUserForm").serializeArray();
    $.ajax({
      type: "POST",
      url: "../admin/controller/ajaxController.php",
      data: data
    })
      .done(function (data) {
        data = $.parseJSON(data);
        if (data.success) {
          $.notify(data.success, 'success');
        }else if (data.warning) {
          $.notify(data.warning, 'warn');
        }
        window.Common.updateUI(1500);
      });
    $("#usersModal").modal('hide');
  });

  /*delete users*/

  /*update users */
  $('button[data-user-id]').on('click' ,function (e) {
    e.preventDefault();
    var userId, login, email, modal;

    userId = this.dataset.userId;
    login = $($(this).parent().parent().find('td')[0]).text();
    email = $($(this).parent().parent().find('td')[1]).text();
    $('#updateUserModal').on('show.bs.modal', function () {
      modal = $(this);
      modal.find('#name').text(login);
      modal.find('[name="updateLogin"]').val(login);
      modal.find('[name="updateEmail"]').val(email);
      modal.find('[name="userId"]').val(userId);
    });
  });

  $('#updateUser').on('click', function () {
    var data, login, email, oldPass, newPass, check;

    login   = $('input[name="updateLogin"]').val();
    email   = $('input[name="updateEmail"]').val();
    oldPass = $('input[name="currentPass"]').val();
    newPass = $('input[name="check"]').val();

    check = checkInputs(login, email,oldPass, newPass);
    if (check[1] === false) {
      /* todo set switch*/
      if (check[2] === 0) {
        $('#updateLogin').css({'border-bottom-color':'red'}).closest('.form-group').addClass('has-error');
      }
      else if (check[2] === 1) {
        $('#updateEmail').css({'border-bottom-color':'red'}).closest('.form-group').addClass('has-error');
      }
      else if (check[2] === 2) {
        $('#checkPass').css({'border-bottom-color': 'red'}).closest('.form-group').addClass('has-error');
      }
      else if (check[2] === 3) {
        $('#checkPass').css({'border-bottom-color': 'red'}).closest('.form-group').addClass('has-error');
      }
    }
    else {
      $('#checkPass').css({
        'border-bottom-color': 'green'
      }).closest('.form-group').removeClass('has-success');
      data = $('#updateUserForm').serializeArray();
      $.ajax({
        type: "POST",
        url: "../admin/controller/ajaxController.php",
        data: data
      })
        .done(function(){
          $('#updateUserModal').modal('hide');
          $.notify('user successfully updated', 'success');
          window.Common.updateUI(1500);
        });
    }
  });

  var checkInputs = function (login, email, oldPass, newPass) {
    var data, emailPattern;
    data = [];
    emailPattern =/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    if (login === undefined || login.length < 2) {
      data.push('login can\'t be empty and must contains 2 symbols at least', false, 0);
    }else if (!emailPattern.test(email)) {
      data.push('please enter a valid email address', false, 1);
    } else if (oldPass > 1 && newPass < 1) {
      data.push('please enter password in check field.', false, 2);
    } else if (oldPass !== newPass) {
      data.push('password does not match', false, 3);
    } else if ((oldPass === newPass) && (oldPass.length > 0) && (newPass.length > 0)) {
      data.push('passwords match', true);
    } else if (oldPass < 1 && newPass < 1) {
      data.push('no need to change password', true);
    }
    return data;
  };

  /*manage footer info
   *
   * need to create universal modal
   * which will send all updates
   */
  $('#systemInfoModal').on('show.bs.modal', function (e) {
    var button, fieldName, id, modal;
    button = $(e.relatedTarget);
    fieldName = button.data('fieldName').replace(/_/g, ' ');
    id = button.data('fieldId');
    modal = $(this);
    modal.find('.modal-title').text(fieldName);
    modal.find('[name="systemInfoId"]').val(id);
  });

  $('.footerData').on('click', function (e) {
    e.preventDefault();
    var data;
    data = $(this).closest('td').prev().text();
    window.tinyMCE.activeEditor.setContent(data);
  });

  $('#addInfo').on('click', function () {
    console.log('hello form modal');
    var formData;
    formData = $('#addSystemInfo').serializeArray();
    formData[0].value = window.tinyMCE.activeEditor.getContent();
    $.ajax({
      type: "POST",
      url: "../admin/controller/ajaxController.php",
      data: formData
    })
      .done(function (data) {
        console.log(data);
        $('#systemInfoModal').modal('hide');
        $('table').fadeOut('slow');
        window.Common.updateUI(1500);
      })
      .fail(function (data) {
        console.log(data);
      });
  });
}
}());