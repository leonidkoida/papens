/**
 * Created by leonid on 28.05.15.
 */
(function(){
  'use strict';
  var console = window.console;

  /*manage project area*/
  if (window.location.pathname === '/admin/manage.php') {

    $('a.status').on('click' ,function () {
      var projectId = this.dataset.id;
      if($(this).hasClass('red')){
        $(this).removeClass('red status-in-progress')
          .addClass('green status-active')
          .html("<i class='fa fa-check' style='color: #ffffff;'></i>");
        $.ajax({
          type:'POST',
          url: '../admin/controller/ajaxController.php',
          data: {'status': [
            {
              'state':1,
              'id': projectId
            }
          ]
          }
        });
        //TODO create status responses
      }
      else if($(this).hasClass('green')) {
        $(this).removeClass('green status-active')
          .addClass('red status-in-progress')
          .html("<i class='fa fa-times' style='color: #ffffff;'></i>");
        $.ajax({
          type:'POST',//PUT
          dataType: 'application/json',
          url: '../admin/controller/ajaxController.php',
          data: {'status': [
            {
              'state':0,
              'id': projectId
            }
          ]
          }
        });
      }
    });

    $(".priority").on('change', function () {
      var projectPriority = $(this).val();
      var projectId = this.dataset.projectId;
      $.ajax({
        type:'POST',//PUT
        dataType: 'application/json',
        url: '../admin/controller/ajaxController.php',
        data: {'priority': [
          {
            'priority': projectPriority,
            'id': projectId
          }
        ]
        }
      });
    });

    $(".delete").on('click', function () {
      var projectId = this.dataset.delete, confirm;
      confirm = window.confirm('delete this project?');
      if (confirm) {
        $.ajax({
          type:'POST',//delete
          dataType: 'application/json',
          url: '../admin/controller/ajaxController.php',
          data: {'project_delete': [
            {
              'id': projectId
            }
          ]
          }
        });
        $(this).closest('tr').remove();
      }
    });

    $('.update').on('click', function (e) {
      e.preventDefault();
      var projectId, modal, systemName, name;

      projectId = this.dataset.update;
      $.getJSON('../admin/controller/ajaxController.php?updateProject=update&id=' + projectId, function (data) {
        var items = data.data.project, i;
        modal = $("#updateProjectModal");
        systemName = items.projectName;
        name = systemName.replace(/_/g,' ');
        modal.find('.modal-title').text('Project "' + name + '"');
        modal.find('#projectName').val(name);
        modal.find('#currentImage').attr(
          {src: '/Assets/img/upload/project_' + systemName + '/project_header_img/'+items.projectImage,
            alt: name
          }
        );
        for (i=0; i<items.creativeFields.length; i++) {
          modal.find('#projectCreative').append('<button class=" m-btn green m-btn-group delete-tag" data-creative-id ="'+ items.creativeFields[i]['id'] +'"> ' +
          items.creativeFields[i]['creative_field'] +
          '<i class="fa fa-times"></button>');
        }

        modal.find('#prDate').val(items.pub_date);
        modal.find('#projectClient').val(items.client);
        if (items.projectDescription !== null) {
          window.tinyMCE.activeEditor.setContent(items.projectDescription);
        }
        modal.find('#projectId').attr('value', projectId);
      });
    });

    $("#updateProject").on('click', function (e) {
      e.preventDefault();
      var form, data, request, options = {}, modal;
      modal = $('#updateProjectModal');

      $("select option:selected").each(function(){
        options[$(this).text()] = $(this).data('creativeId');
      });
      options = JSON.stringify(options);
      form = document.forms.namedItem('updateProjectForm');
      data = new FormData(form);
      data.append('projectDescription', window.tinyMCE.activeEditor.getContent());
      data.append('creative', options);
      request = new XMLHttpRequest();
      request.open("POST", "../admin/controller/ajaxController.php", true);
      request.onload = function (e) {
        if (request.status === 200) {
          $.notify('data is updated', 'success');
          modal.modal('hide');
          modal.on('hidden.bs.modal', function(){
            window.Common.updateUI(1500);
          });
        }else {
          console.log('no result');
        }
      };
      request.send(data);
    });

    $('#projectCreative').on('click', '.delete-tag', function (e) {
      e.preventDefault();
      var cId, pId;
      pId = $('#updateProjectForm').find('#projectId').val();
      cId = this.dataset.creativeId;

      $.ajax({
        type: "POST",
        url: '../admin/controller/ajaxController.php',
        data: {
          deleteTagFromProject: {
            projectId: pId,
            tagId: cId
          }
        }
      }).done(function (data) {
        data = $.parseJSON(data);
        console.log(data.data.id);
        var span = $('#projectCreative').find('[data-creative-id=' + data.data.id + ']');
        $(span).fadeOut().remove();
      });
    });

    $("#updateProjectModal").on('hidden.bs.modal', function () {
      $(this).find('#projectCreative').empty();
    });
  }
}());