/**
 * Created by leonid on 28.05.15.
 */
(function(){
  "use strict";
  /*manage personal area*/
  if (window.location.pathname === '/admin/personal.php') {
    //just for testing
    var console = window.console;
    var uploadPersonImage = function (input, options) {
      var Data, req;
      options = options || {};
      Data = new FormData(input);
      Data.append('img', options);
      req = new XMLHttpRequest();
      req.open('POST', '../admin/controller/ajaxController.php', true);
      req.onload = function (e) {
        var data = $.parseJSON(req.response);
        if (req.status === 200) {//TODO  modify to good view and try to send different status codes in data
          $.notify(data.data, 'success');
          window.Common.updateUI(1000);
        }else {
          $.notify(data.data, 'error');
        }
      };
      req.send(Data);
    };
    // TODO change code to DRY
    $('#upload-header-img').on('click', function (e) {
      e.preventDefault();
      var input, img;
      input = document.forms.namedItem('uploadHeaderImg');
      img = 'headerImage';
      uploadPersonImage(input, img);
    });

    $('#upload-about-image').on('click', function (e) {
      e.preventDefault();
      var input, img;
      input = document.forms.namedItem('uploadAboutImg');
      img = 'aboutImage';
      uploadPersonImage(input, img);
    });

    $("#upload-icon-image").on('click', function (e) {
      e.preventDefault();
      //TODO need to move all validation to separate function and add it to all inputs type file
      var parent, img, type, data, input, icon;
      //TODO check the type of the image
      parent = $(this).parent().parent();
      img = parent.find('input[type="file"]').val();
      if (img.length > 0) {
        img = img.match(/(\w+:\\\w+\\\w+.)(\w+)/);
        type = img[2];
        data = window.Common.checkImageType(type);
        if (data[0] === false) {
          parent.find('.fileUploaderInput').removeClass('alert-success').addClass('alert-danger');
          parent.find('.img_path').text('Wrong type');
          parent.find('.imageIndicator').css({'background-color':'red'});
          $.notify(data[1], 'error');
        }
        else{
          parent.find('.fileUploaderInput').removeClass('alert-danger').addClass('alert-success');
          parent.find('.imageIndicator').css({'background-color':'green'});
          input = document.forms.namedItem('uploadIcon');
          icon = 'iconImage';
          uploadPersonImage(input, icon);
        }
      }
      else{
        parent.find('.fileUploaderInput').removeClass('alert-success').addClass('alert-danger');
        $.notify('you try to send empty data', 'error');
      }
    });
// TODO ---------------------------------

    $('#descriptionModal').on('show.bs.modal', function (e) {
      $.getJSON('../admin/controller/ajaxController.php?description=about', function (json) {
        //$('textarea').val(json.data.description);
        window.tinyMCE.activeEditor.setContent(json.data.description);
      });
    });

    $("#addDescription").on('click', function (e) {
      e.preventDefault();
      var data;
      data = window.tinyMCE.activeEditor.getContent();
      $.ajax({
        type: "POST",
        url: "../admin/controller/ajaxController.php",
        data: {
          aboutDescription: 'about',
          data: data
        }
      }).done(function (data) {
        data = $.parseJSON(data);
        data = data.data.success;
        $.notify(data, 'success');
        $('#descriptionModal').modal('hide');
      }).fail(function (data) {
        var response, text;
        response = $.parseJSON(data.responseText);
        response = response.data.error;
        $.notify(response, 'error');
      });
    });

    // append new link name to DOM
    var appendLinkName = function (name) {
      var link, linkAddr, linkName, lastRow;
      link = name.data.name;
      linkAddr = name.data.link;
      lastRow = $('.s_links>.row').last();
      linkName = $('<div class="row" style="margin-bottom: 5px;">'+
      '<div class="col-md-2" style="overflow: hidden;"><label class="field">' + link + '</label> </div>'+
      '<div class="col-md-6"><p style="max-width:216px; overflow:hidden;" >' + linkAddr + '</p></div>'+
      '<div class="col-md-4">' +
      '<div class="btn-group">'+
      '<button class="m-btn m-btn-group blue updater">' +
      '<i class="fa fa-check"></i>' +
      '</button> ' +
      '<button class="m-btn m-btn-group red deleter">' +
      '<i class="fa fa-times"></i>' +
      '</button>' +
      '</div>' +
      '</div>'+
      '</div>');
      $('.s_links').fadeOut(
        100,
        function () {
          linkName.insertAfter(lastRow);
          $('.s_links').fadeIn(100);
        }
      );
    };

    $('#save-social').on('click', function () {
      var data, dataSuccess;
      data = document.forms.namedItem('socialLinksForm');
      data = $(data).serializeArray();
      $.ajax({
        type:'POST',
        url: '../admin/controller/ajaxController.php',
        data: data
      })
        .done(function (data) {
          data = $.parseJSON(data);
          console.log(data);
          dataSuccess = data.data.success;
          appendLinkName(data);
          $('#socialLink').modal('hide');
          $.notify(dataSuccess, 'success');
        })
        .fail(function (data) {
          var response;
          response = $.parseJSON(data.responseText);
          response = response.data.error;
          $.notify(response, 'error');
        });
    });

    $('.updater').on('click', function (e) {
      e.preventDefault();
      var linkId, linkName, link;
      linkId = $(this).parent().parent().prev().find('p').data('linkId');
      linkName = $(this).parent().parent().prev().prev().find('label').text();
      link = $(this).parent().parent().prev().find('p').text();

      $('#UpdateLinkModal').on('show.bs.modal', function (e) {
        var modal;
        modal = $(this);
        modal.find('#linkName').val(linkName);
        modal.find('#linkAddress').val(link);
        modal.find('[name="linkId"]').val(linkId);
      });
    });

    $('#UpdateLink').on('click', function (e) {
      e.preventDefault();
      var data;
      data = $('#UpdateLinkForm').serializeArray();
      $.ajax({
        type: "POST",
        url: "../admin/controller/ajaxController.php",
        data: data
      })
        .done(function (data) {
          var linkName, linkAddress;
          data = $.parseJSON(data);
          console.log(data);
          linkName = data.data.name;
          linkAddress = data.data.address;
          $('#UpdateLinkModal').modal('hide');
          $('.s_links').fadeOut(
            'fast',
            function () {
              $.notify('link ' + linkName +' '  + linkAddress + 'was updated', 'success');
              //todo think how to avoid this behavior
              window.Common.updateUI(500);
            }
          );

        });
    });

    $('.deleter').on('click', function(e){
      e.preventDefault();
      removeLink(this);
    });

    var removeLink = function (link) {
      var parent, linkId;
//TODO create a confirm for delete
      parent = $(link).parent().parent().parent();
      linkId =  $(parent).find('p').data('linkId');

      $.ajax({
        type: "POST",
        url : "../admin/controller/ajaxController.php",
        data: {
          removeLink: linkId
        }
      })
        .done(function (data) {
          data = $.parseJSON(data);
          data = data.data.success;
          $.notify(data, 'success');
        });
      $(parent).fadeOut(500, function () {
        $(parent).remove();
      });
    };
  }
  /* finish of personal*/
})();