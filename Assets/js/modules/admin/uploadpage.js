/**
 * Created by leonid on 28.05.15.
 */
(function(){
  'use strict';
  /*manage upload page*/
  if (window.location.pathname === "/admin/upload.php") {
    $("[name='saveProject']").on('click', function (e) {
      e.preventDefault();
      var data, options = {};
      $("select option:selected").each(function(){
        options[$(this).text()] = $(this).data('creativeId');
      });
      data = document.forms.namedItem('uploadProject');
      debugger;
      window.Common.uploadImage(data, options);
      window.Common.updateUI(1500);
    });

    /*manage description textarea view in upload project area form*/
    $("#prDescription").on('click', function (e) {
      e.preventDefault();
      $("#description-container").slideToggle('slow');
    });

    $('#uploadProjectItems').on('click', '.reset', function (e) {
      e.preventDefault();
      $(this).parent().parent().fadeOut({
        duration: 500,
        easing: 'easeInQuint',
        complete: function () {
          $(this).remove();
        }
      });
    });

    $("#sendProjectItems").on('click', function (e) {
      e.preventDefault();
      var data = document.forms.namedItem('uploadProjectItems');
      window.Common.uploadImage(data);
      window.Common.updateUI(1500);
    });
  }
})();
