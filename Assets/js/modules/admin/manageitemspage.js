/**
 * Created by leonid on 28.05.15.
 */
(function(){
  'use strict';
  var console = window.console;
  /*manage project items area*/
  if (window.location.pathname === '/admin/manageItems.php') {

      /**
       * set order of show items on project page
       */
    $('input.priority').on('change', function () {
      var itemId, dataSrc, priority;
      dataSrc = $(this).parent().parent().next().attr('class');
      console.log(dataSrc);
      itemId = this.dataset.priority;
      priority = this.value;

      $.ajax({
        type: "POST",
        url: '../admin/controller/ajaxController.php',
        data: {
          itemPriority : {
            id: itemId,
            data: dataSrc,
            priority: priority
          }
        }
      })
          .done(function (data) {
              data = $.parseJSON(data);
              $.notify(data.data, 'success');

          });
    });

    $('.status-button').on('click', function (e) {
      e.preventDefault();
      var itemId, currentStatus, dataSrc;
      itemId = this.dataset.status;
      dataSrc = $(this).parent().parent().next().attr('class');
      currentStatus = 1;
      if ($(this).hasClass('green')) {
        currentStatus = 0;
        $(this).removeClass('green').addClass('red');
        $(this).find('i').removeClass('fa-check').addClass('fa-times');
      }else {
        currentStatus = 1;
        $(this).removeClass('red').addClass('green');
        $(this).find('i').removeClass('fa-times').addClass('fa-check');
      }

      $.ajax({
        type: "POST",
        url: "../admin/controller/ajaxController.php",
        data: {
          itemStatus : {
            id: itemId,
            data: dataSrc,
            status: currentStatus
          }
        }
      });
    });

    $("#descriptionModal").on( 'show.bs.modal', function (e) {
      var itemId, target, modal, hidden, dataSrc, text;
      target = e.relatedTarget;
      itemId = target.dataset.description;
      dataSrc = $(target).parent().parent().next().attr('class');
      modal = $(this);
        //FIXME rename updateItemDescription to getItemDescription
      $.getJSON('../admin/controller/ajaxController.php?updateItemDescription=' + itemId + '&src=' + dataSrc, function (data) {
        text = data[0].description;
        if (text !== null) {
          window.tinyMCE.activeEditor.setContent(data[0].description);
        }
      });
      modal.find('form').attr({id: "addItemDescription"});
      modal.find('textarea').attr({name: "itemDescription"});
      modal.find('#name').text("#" + itemId);
      hidden = modal.find('[type="hidden"]');
      if (hidden.length > 1) {
        $(hidden[0]).remove();
        $(hidden[1]).attr({
          'name': "itemId",
          'value': itemId,
          'data-data-Src': dataSrc
        });
      }else {
        $(hidden[0]).attr({
          name: "itemId",
          value: itemId
        });
      }
    });

    $("#addDescription").on('click', function () {
      var form, data, dataSrc, Src= {}, text, identificator ={}, description = {};
      form = $("#addItemDescription");
      data = form.serializeArray();
        text = window.tinyMCE.activeEditor.getContent();
        dataSrc = form.find('[type="hidden"]').data('dataSrc');

        description.name = 'itemDescription';
        description.value = text;

        identificator.value = true;
        identificator.name = $(form).attr('id');

        Src.name = 'src';
        Src.value = dataSrc;

        data.push(description);
        data.push(Src);
        data.push(identificator);
        $.ajax({
        type: "POST",
        url: "../admin/controller/ajaxController.php",
        data: data
      })
        .done(function(data){
                data  = $.parseJSON(data);
                $.notify(data, 'success');
          $("#descriptionModal")
            .modal('hide')
            .on('hidden.bs.modal', function () {
              window.tinyMCE.activeEditor.setContent('');
            });
        });
    });

    $('.delete-button').on('click', function (e) {
      e.preventDefault();
      var conf, itemId, _this, dataSrc;
      _this = this;
      itemId = this.dataset.delete;
      dataSrc = $(this).parent().parent().next().attr('class');
      conf = window.confirm('delete item?');
      if (conf) {
        $.ajax({
          type: "POST",
          url: "../admin/controller/ajaxController.php",
          data: {
            itemDelete : {
              id: itemId,
              data: dataSrc
            }
          }
        }).done(function (data) {
          data = $.parseJSON(data);
          $.notify(data, 'success');
          $(_this).parent().parent().parent().fadeOut('slow',function(){
            $(_this).remove();
          });

        });
      }
    });
  }
}());