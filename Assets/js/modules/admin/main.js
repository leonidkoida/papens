/**
 * Created by leonid on 09.08.15.
 */
  var Common;
  Common = {

    updateUI: function(options){
      'use strict';
      options = options || {};
     return setTimeout(function(){
          window.location.reload();
        } ,
        options);
    },

    uploadImage: function (input, options) {
      "use strict";
      options = options || {};
      options = JSON.stringify(options);
      var description, Data, req;

      //description = $('#prDescr').val();
        description = window.tinyMCE.activeEditor.getContent();
      debugger;
      Data = new FormData(input);
      Data.append('projectDescription', description);
      Data.append('creative', options);
      req = new XMLHttpRequest();
      req.open('POST', '../admin/controller/ajaxController.php', true);
      req.onload = function (ev) {
        window.console.log(ev);
        if (req.status === 200) {
          $.notify('Project was successfully uploaded', 'success');
        }else {
          $.notify('Error '+ req.status + ' is happened during upload of project', 'error');
        }
      };
      req.send(Data);
    },

    checkImageType: function(img){
      "use strict";
      var data = [];
      switch (img) {
        case 'jpg': data[0] = false;
                    data[1] = 'this image is in jpg format, accept only .ico';
        break;

        case 'jpeg': data[0] = false;
                     data[1] = 'this image is in jpeg format, accept only .ico';
        break;

        case 'png': data[0] = true;
                    data[1] = 'this image is in png format, accept only .ico';
          break;

        case 'gif': data[0] = false;
                    data[1] = 'this image is in gif format, accept only .ico';
          break;
        default : data[0] = true;
      }
     return data;
    },

    showIndicator: function(_this, width){
      "use strict";
      var indicator, el;
      el = $(_this);
      indicator = el.next();
      indicator.animate({'width': width }, 500, 'swing');
  },

    addInputField: function () {
     "use strict";
      var inputBlock;

      inputBlock = $('<div class="row">'+
      '<div class="col-md-3"></div>'+
      '<div class="col-md-8"><label class="alert-success fileUploaderInput">'+
      '<span>'+
      '<i class="fa fa-file-image-o" style="font-size: large;"></i>'+
      '<span class="img_path">Select image</span>'+
      '</span>'+
      '<input type="file" name="prItemImage[]" accept="image" style="display:none;"/>'+
      '<span class="imageIndicator"></span>'+
      '</label></div>'+
      '<div class="col-md-1">' +
      '<button class="btn btn-sm btn-danger reset"><i class="fa fa-remove"></i></button>'+
      '</div>'+
      '</div>');
      $('#uploadProjectItems').append(inputBlock);
  },

     addLink: function(){
       "use strict";
       var link = $('<div class="row">'+
       '<div class="col-md-3"></div>'+
       '<div class="col-md-8"><label class="linkUploaderInput">'+
       '<span>add Link </span>'+
       '<input type="text" name="link[]"/>'+
       '<span class="imageIndicator"></span>'+
       '</label></div>'+
       '<div class="col-md-1">' +
       '<button class="btn btn-sm btn-danger reset"><i class="fa fa-remove"></i></button>'+
       '</div>'+
       '</div>');
       $('#uploadProjectItems').append(link);
  }
  };

// common logic for all project
(function(){
  "use strict";
  $('form').on('change', 'input[type="file"], input[type="text"]', function(){
    var path, pattern, width, el;
    path = $(this).val();
    pattern = /(\w)(:)(\\\w+?\\)/gi;
    el = $(this);
    path = path.replace(pattern, '');
    width = el.parent().width();
    el.parent().find('.img_path').text(path);
    Common.showIndicator(el, width);
  })
    .on('reset', function() {
      var el;
      el = $(this);
      el.parent().find('.img_path').text('Select image');
      el.parent().find('input[type="file"]').val('');
    });

  //init datapicker
  $('#prDate').datepicker({
    format        : "dd/mm/yyyy",
    language      : "ru",
    orientation   : "top auto",
    todayHighlight: true,
    autoclose     : true
  });

  //init tooltips
  $('[data-toggle="tooltip"]').tooltip();
})();


//some light customisation for editor width
var editorWidth = 568;
if(window.location.pathname === '/admin/upload.php'){
    editorWidth = 400;
}
//initialize plugin for text areas
window.tinymce.init({
  selector: ".text-editor",
  width: editorWidth,
  plugins: [
    "advlist autolink lists charmap preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media contextmenu paste"
  ],
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify",
  formats : {
    alignleft : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'left'},
    aligncenter : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'center'},
    alignright : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'right'},
    alignfull : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'full'},
    bold : {inline : 'span', 'classes' : 'bold'},
    italic : {inline : 'span', 'classes' : 'italic'},
    underline : {inline : 'span', 'classes' : 'underline', exact : true},
    strikethrough : {inline : 'del'},
    forecolor : {inline : 'span', classes : 'forecolor', styles : {color : '%value'}},
    hilitecolor : {inline : 'span', classes : 'hilitecolor', styles : {backgroundColor : '%value'}}
  }
});


