(function(){
  "use strict";
  $(function() {
  $('#exampleModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var recipient = button.data('whatever');

    recipient = recipient.toUpperCase();
    var modal = $(this);
   modal.find('.modal-title').text(recipient);
    modal.find('.control-label').text(recipient);
    modal.find('[name="tagControl"]').val(recipient);
  });

  $("#addInput").on('click', function () {
      window.Common.addInputField();
  });

  $('#addLink').on('click', function (e) {
    e.preventDefault();
    window.Common.addLink();
  });
});
})();
