<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 26.04.15
 * Time: 19:52
 */

require_once 'ConnectionController.php';
/**
 * Class footerController
 */
class footerController extends ConnectionController
{


    public $newFooter = [];

    /**
     * @return array|string
     * @throws Exception
     */
    public function getData()
    {
        $result = $this->setMysqli();
        $query = $result->query("SELECT * FROM SystemText ORDER BY id ASC");
        if ($query->num_rows > 0) {
            while ($row = $query->fetch_assoc()) {
                $this->newFooter[] = $row;
            }
        } else {
            $this->newFooter[] = 'error';
        }
        $result->close();
        return $this->newFooter;
    }
}
