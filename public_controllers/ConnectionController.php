<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 28.03.15
 * Time: 22:12
 */

//namespace public_controllers;

require_once 'connections/connections.php';


/**
 * Class ConnectionController
 */
class ConnectionController
{

    public $mysqli;

    /**
     * @return mysqli
     * @throws Exception
     */
      public function setMysqli()
      {
        $mysqli = $this->mysqli = new mysqli(HOST, USER, PASS, DB_NAME);

        if ($mysqli->connect_error) {
              throw new Exception('no connection with database');
        } else {
              return $mysqli;
        }
      }
}
