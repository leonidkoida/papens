<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 14.07.15
 * Time: 20:51
 */

require '../connections/connections.php';
require '../vendor/autoload.php';


/**
 * Class MailController
 */
class MailController
{

    public $data = [];

    public $mysqli;

    /**
     * @return mysqli
     * @throws Exception
     */
    public function setMysqli()
    {
        $mysqli = $this->mysqli = new mysqli(HOST, USER, PASS, DB_NAME);

        if ($mysqli->connect_error) {
            throw new Exception('no connection with database');
        } else {
            return $mysqli;
        }
    }

    /**
     * @param $email
     * @return string
     */
    public function sendMail(array $email)
    {
        $name    = trim(htmlspecialchars($email['userName']));
        $address = trim(htmlspecialchars($email['userEmail']));
        $text    = htmlspecialchars($email['userText']);

        $check = $this->checkEmail($address);
        $result = $this->setMysqli();

        if ($check == true) {
            $request = $result->query("INSERT INTO user_mails(user_name, user_email, user_message)
                                      VALUES ('$name', '$address', '$text')");
            if ($request) {
                $this->data[] = 'message was saved in database successfully';
                $this->phpMailer($name, $address, $text);
            } else {
                $this->data[] = 'can\'t save the message';
            }
        }

        $result->close();
        return json_encode($this->data);
    }

    /**
     * PHP mailer class used here to send emails
     *
     * @param $name
     * @param $address
     * @param $text
     * @return bool
     */
    public function phpMailer($name, $address, $text)
    {
        $email = new PHPMailer();
        $email->setLanguage('ru', '/phpmailer/language/');
        $finalAddress = self::getFinalAddress();

        $email->setFrom($address, $name);
        $email->addAddress($finalAddress);
        $email->Subject = 'PHPMailer mail() test';
        $email->Body = $text;
        $email->AltBody = $text;

        if (!$email->send()) {
                $this->data[] = 'Mailer Error: ' . $email->ErrorInfo;
        } else {
                $this->data[] = 'Message was successfully sent!';
        }
        return $this->data;
    }

    private function getFinalAddress()
    {
        $result = [];
        $query = $this->setMysqli();

        $query = $query->query("SELECT `name`
                                FROM SystemText
                                WHERE `system_name` = 'test_email' LIMIT 1");
        if ($query->num_rows>0) {
            while ($row = $query->fetch_assoc()) {
                $result = $row;
            }
        }
        $result = $result['name'];
        return $result;
    }

    /**
     * @param $email
     * @return string
     */
    public function checkEmail($email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->data['error'] = 'invalid email';
        } else {
            return true;
        }
    }

    /**
     * Will try latter SWIFT mailer
     *
     * @param $name
     * @param $address
     * @param $text
     * @return array
     */
    /*    public function swiftMailer($name, $address, $text)
        {
            return $this->data;
        }*/
}
