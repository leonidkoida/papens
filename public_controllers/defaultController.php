<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 28.03.15
 * Time: 22:11
 */

require_once 'ConnectionController.php';
require 'vendor/swiftmailer/swiftmailer/lib/swift_required.php';

class defaultController extends ConnectionController
{

    /**
     * @return array
     * @throws Exception
     */
    private $path = 'Assets/img/upload/';

    public $image_array = [];

    public $tags_array =[];

    public $links_array = [];

    public function getPath($path)
    {
        return $this->path = $path;
    }

    /**
     * @return array|bool|string
     * @throws Exception
     */
    public function getIcon()
    {
        $connect = $this->setMysqli();

        $result = $connect->query("SELECT icon_image_url
                                   FROM PersonalInfo LIMIT 1");
        $icon = '';
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $icon = $row;
            }
        } else {
            $icon = false;
        }
        $connect->close();
        return $icon;
    }

    public function getTitles($title)
    {
        $defaultTitle = 'studiopape.com';
        $title = $defaultTitle;
        return $title;
    }

    /**
     * @return array $menu
     * @throws Exception
     */
    public function test()
    {
        $connect = $this->setMysqli();
        $result = $connect->query('SELECT * FROM t_Project WHERE status=1 ORDER  BY priority');
        $menu = [];
        while ($row = $result->fetch_assoc()) {
            $row['creativeFields'] = [];
            $tags = $connect->query("
            SELECT c.creative_field
            FROM Creative_Fields c  JOIN index_creative i
            On i.creative_field_id = c.id
            WHERE i.project_id = '$row[id]'
            ");
            if ($tags->num_rows>0) {
                while ($tag = $tags->fetch_assoc()) {
                    array_push($row['creativeFields'], $tag);
                }
            } else {
            }
            $menu[] = $row;
        }
        $connect->close();
        return $menu;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getAboutImage()
    {
        $img = [];
//        $row = '';
        $getImg = $this->setMysqli()->query("SELECT about_image_url FROM PersonalInfo");
        if ($getImg->num_rows > 0) {
            while ($row = $getImg->fetch_assoc()) {
                $img[] = $row;
            }
        } else {
            $img[] = false;
        }
        $getImg->close();
        return $img;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getHeaderImage()
    {
        $img = [];
        $getImg = $this->setMysqli()->query("SELECT header_image_url FROM PersonalInfo");
        if ($getImg->num_rows > 0) {
            while ($row = $getImg->fetch_assoc()) {
                $img[] = $row;
            }
        } else {
            $img[] = false;
        }
        $getImg->close();
        return $img;

    }

    /**
     * @return array
     * @throws Exception
     */
    public function getLinkMenu()
    {
        $connect = $this->setMysqli()->query("SELECT * FROM social_links ORDER BY id");
        $menu = [];
        while ($row = $connect->fetch_assoc()) {
            $menu[] = $row;
        }
        $connect->close();
        return $menu;
    }

    /**
     * @param  $projectName
     * @return array
     * @throws Exception
     */
    public function getProjectItems($projectName)
    {
        $this->image_array=[];

        $connect = $this->setMysqli();
        $result = $connect->query("SELECT id FROM t_Project WHERE projectName='$projectName'");

        if ($result->num_rows!=0) {
            $id = $result->fetch_assoc();
            $id = $id['id'];
            $path_pattern = 'project_'.$projectName;
            if (file_exists(PATH_TO_UPLOAD.'/'.$path_pattern.'/items')) {
                $items = $connect->query("SELECT I.image_url, I.description, I.priority, P.projectName, P.client,
                                         YEAR(P.pub_date) as date, P.projectDescription
                                        FROM ProjectItems I INNER JOIN t_Project P
                                         ON I.project_id = P.id
                                        WHERE I.project_id='$id' AND I.status = 1");

                $links = $connect->query("SELECT v.link, v.priority, v.description
                                          FROM Video_links v INNER JOIN t_Project P
                                          ON v.project_id = P.id
                                           WHERE v.project_id='$id' AND v.status = 1");

                $tags  = $connect->query("SELECT DISTINCT c.creative_field FROM Creative_Fields c
                                                    INNER JOIN index_creative i
                                                    ON i.creative_field_id = c.id
                                                     WHERE i.project_id = '$id'");


                while ($item = $items->fetch_assoc()) {
                    $this->image_array[] = $item;
                }

                if ($links->num_rows>0) {
                    while ($link = $links->fetch_assoc()) {
                        $this->links_array[] = $link;
                    }
                }

                if ($tags->num_rows>0) {
                    while ($tag = $tags->fetch_assoc()) {
                        $this->tags_array[] = $tag;
                    }
                }

                $connect->close();
            } else {
                echo "no such file or directory ". PATH_TO_UPLOAD.'/'.$path_pattern.'/items';
            }
        }
        $this->image_array = array_merge($this->image_array, $this->links_array);

        usort($this->image_array, function ($item, $link) {
            return strcmp($item['priority'], $link['priority']);
        });

        return ['items' => $this->image_array, 'tags' => $this->tags_array];
    }

    /**
     * @param $tag
     * @return array|bool|mysqli_result
     * @throws Exception
     */
    public function getProjectByTag($tag)
    {
        $getProjectsByTag = $this->setMysqli();
            $getProject = $getProjectsByTag->query("
            SELECT p.id, p.projectName,p.pub_date,p.client, p.status, p.priority, p.projectDescription, p.projectImage
            FROM t_Project p INNER JOIN index_creative i
            ON i.project_id = p.id
            INNER JOIN Creative_Fields c
            ON c.id = i.creative_field_id
            WHERE c.creative_field='$tag'");
        if ($getProject->num_rows > 0) {
            $tagMenu = [];
            while ($row = $getProject->fetch_assoc()) {
                $row['creativeFields'] = [];

                //TODO set this part of query into helper Query class
                $tags = $getProjectsByTag->query("
            SELECT c.creative_field, c.id
            FROM Creative_Fields c  JOIN index_creative i
            On i.creative_field_id = c.id
            WHERE i.project_id = '$row[id]'
            ");
                if ($tags->num_rows>0) {
                    while ($tag = $tags->fetch_assoc()) {
                        array_push($row['creativeFields'], $tag);
                    }
                } else {
                    $row['creativeFields'] = [0=>['creative_field' => 'no tags']];
                }

                $tagMenu[] = $row;
            }
            $getProjectsByTag->close();
            return $tagMenu;
        } else {
            echo 'no project for this tag';
        }

        return $getProjectsByTag;
    }

    /**
     * @return array|string
     * @throws Exception
     */
    public function getAboutDescription()
    {
        $result = $this->setMysqli()->query("SELECT description FROM PersonalInfo LIMIT 1");
        if ($result->num_rows > 0) {
            $description = '';
            while ($row = $result->fetch_assoc()) {
                $description = $row;
            }
        } else {
            $description = 'no description yet.';
        }
        $result->close();
        return $description;
    }

    public function sendMail($post)
    {
        $user = trim(htmlspecialchars($post['userName']));
        $email = trim(htmlspecialchars($post['userEmail']));
        $msg = htmlspecialchars($post['userText']);

        $result = $this->setMysqli()->query("
                INSERT INTO `user_emails`(`user_name`, `user_email`, `user_text`)
                 VALUES ('$user', '$email', '$msg')");
        if ($result) {
            $msg = 'success';
//            $this->swift();
        }else{
            $msg = 'error';
        }
        return $msg;
    }

    public function swift()
    {
//        $transport = Swift_MailTransport::newInstance();
//        $transport = \Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -t');
//
//        $mailer = \Swift_Mailer::newInstance($transport);

// Create a message
//        $message = \Swift_Message::newInstance()
//            ->setSubject('Wonderful Subject')
//            ->setFrom(array('john@doe.com' => 'John Doe'))
//            ->setTo(array('leonidkoida@gmail.com', 'k_leon_m@mail.ru' => 'A name'))
//            ->setBody('Here is the message itself')
//        ;

// Send the message
//        $result = $mailer->send($message);

    }

    /**
     * @return array|string
     * @throws Exception
     */
    public function getMetaDescription()
    {
        $meta_description = '';
        $result = $this->setMysqli()->query("SELECT system_name, name FROM SystemText WHERE system_name LIKE '%meta_description%' LIMIT 1");

        if ($result->num_rows >0) {
            while ($row = $result->fetch_assoc()) {
                $meta_description = $row;
            }
        }
        if ($meta_description['name'] == "") {
            $meta_description['name'] = "Design, Logo, Images";
        }

        $result->close();
        return $meta_description;
    }
}
