<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 11.03.15
 * Time: 23:36
 * depending on which project was clicked that project will rendering
 */
include_once "templates/header.php";

$project_name = new defaultController();
if (isset($_GET['project'])) {
    $name = trim(htmlspecialchars($_GET['project']));
    $projectName = preg_replace('/_/', ' ', $name);
    $items = $project_name->getProjectItems($_GET['project']);
?>
    <!--<div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>-->
<div class="row">
    <div class="col-md-7 col-md-offset-2">
                <h1 class="project-title"><?php echo $projectName; ?></h1>

                <p class="project-data">
                    <span >
                        Date: <?php echo $items['items'][0]['date']; ?> -
                    </span>
                    <span style="color: rgba(134, 135, 133, 0.94)"> Client: <?php echo $items['items'][0]['client'];?>
                    </span>
                </p>
                <p class="link link-tag">
                    <?php
                    $tags = $items['tags'];
                    $counter = 0;
                    $i = count($tags);
                    foreach ($tags as $tag) {
                        echo "<a class='link' href='tag.php?tag=" .
                            $tag['creative_field'] . "'>" .
                            $tag['creative_field'];
                        if ($counter != $i- 1) {
                            echo ', ';
                        }
                        echo "</a>";
                        ++$counter;
                    }
}
                    ?>
                </p>

        <div class="row">
            <div class="col-md-12">
                <?php
                foreach ($items['items'] as $item) {
                    if ($item['link']) {
                        echo "<iframe src = '" . $item['link'] . "' frameborder='0' allowfullscreen></iframe>";
                    } else {
                        echo "<img src='/".PATH_TO_UPLOAD."/project_".$name."/items/".$item['image_url']."'/>";
                    }

                    if (!isset($item['description'])) {
                        echo "<p class='alert-info' style='margin-bottom: 20px;'></p>";
                        continue;
                    } else {
                        echo "<div class='item-description'>";
                        echo $item['description'];
                        echo "</div>";
                    }
                }
                ?>
            <div class="project-description">
                <?php
                if (!isset($items['items'][0]['projectDescription'])) {
                } else {
                    echo $items['items'][0]['projectDescription'];
                }

                // TODO this is idea to regex for youtube video \w{4,5}:\/\/\w{3}\.\w+\.\w{3}\/\w+\?\w=(\w+)
                // TODO create a check on frontend to correct link
?>
         </div>
        </div>
            <div class="col-md-12 social">
                <div class="fb-share-button" data-share="true" data-layout="button">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <?php include_once "templates/sidebar.php"; ?>
    </div>
</div>
<?php
include_once "templates/footer.php";
?>