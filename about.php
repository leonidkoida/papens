<?php
include_once 'templates/header.php';

$systemText = new footerController();
$text = $systemText->getData();
$aboutImg = $links->getAboutImage();
$socialLinks = $links->getLinkMenu();
$description = $links->getAboutDescription();
if (!empty($_POST)) {
    $links->sendMail($_POST);
}
?>
<div class="row">

    <div class="col-md-7 col-md-offset-2">
        <div class="row">
            <div class="col-md-4">
                <!--  TODO set the dinamic way to file-->
                <?php
                $img = $aboutImg[0]['about_image_url'];
                if ($img == false) {
                    echo "<img src='/" . PATH_TO_SYSTEM_IMG . "/imgAbout/default/default.gif' class='img-circle'/>";
                } else {
                    echo "<img src='/" .PATH_TO_SYSTEM_IMG . "/imgAbout/" . $img . "' class='img-circle' />";
                }
                ?>
            </div>
            <div class="col-md-8">
                <div class="description-about">
                <?php
                echo $description['description'];
                ?>
                </div>
                <hr/>
                <a href="index.php" class='link'>
                    <?php echo $text[1]['name']; ?>
                </a>

                <div class="get-in-touch">
                        <h3><?php echo $text[3]['name']; ?></h3>
                    <ul class="list-unstyled">
                        <li><?php echo $text[4]['name'] ?></li>
                        <?php
                        $i = 0;
                        foreach ($socialLinks as $linkItem) {
                            // TODO change href attribute? because if they will set url like this: http://anyname.com it will break all
                            echo "<li>";
                            echo "<a class='link' target='_blank' href='" . $linkItem['link'] . "'>" . $linkItem['name'] . "</a>";
                            echo "</li>";

                            $i++;
                        }
                        ?>
                    </ul>
                    <h3><?php echo $text[5]['name']; ?></h3>
                    <form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <input placeholder="Name" name="userName" style="width: 100%;" type="text"/>
                            </div>
                            <div class="col-md-6">
                                <input placeholder="Email" name="userEmail" style="width: 100%;" type="text"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <textarea placeholder="Message" name="userText" id="" cols="3" rows=""></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="submit" id="send_mail" class="m-btn m-btn-group small blue">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <?php include_once "templates/sidebar.php" ?>
    </div>
</div>
<?php
include_once "templates/footer.php";
?>