<?php
include_once 'templates/header.php';
?>
<div class="row">
    <div class="col-md-7 col-md-offset-2">
        <?php
        if (isset($_GET['tag'])) {
            $tag = trim($_GET['tag']);
            $images = new defaultController();
            $tags = $images->getProjectByTag($tag);
            $counter = 1;

            echo "<div class='row main-row'>";
            foreach ($tags as $img) {
                $name = preg_replace('/_/', ' ', $img['projectName']);
                echo "<div class='col-md-6' style=''>
                  <a href='project.php?project=".$img['projectName']. "'>
                  <div class='figure'>
                    <p>
                     <img class='project-image' style='max-height:210px;' src=' /" .
                     PATH_TO_UPLOAD."/project_" .
                     $img['projectName'].'/project_header_img/' .
                     $img['projectImage'] . "'/>
                     </p>
                     <div class='figcaption'>
                     <h3 class='project-name'>".$name."</h3></a>
                     <p>";
                $countTags = 0;
                $i = count($img['creativeFields']);
                foreach ($img['creativeFields'] as $tag) {
                    echo "<a class='link link-tag' href='tag.php?tag=" .
                    $tag['creative_field']."'>".$tag['creative_field'];
                    if ($countTags != $i- 1) {
                        echo ', ';
                    }
                    echo "</a>";
                    ++$countTags;
                }
                echo "</p>
                     </div>
                     </div>

                  </div>";
                if ($counter%2==0) {
                    echo "</div><div class='row' >";
                }
                $counter++;
            }
                echo "</div>";
        }
        ?>
    </div>
    <div class="col-md-2">
        <div class="nav-module">
            <?php
            include "templates/sidebar.php";
            ?>
        </div>
    </div>
</div>
<?php include_once "templates/footer.php"; ?>

