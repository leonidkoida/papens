<?php
//fixme recreate image container
include_once 'templates/header.php';
?>
<div class="row">
    <div class="col-md-7 col-md-offset-2">
        <?php
        $images = new defaultController();
        $front_img = $images->test();
        $counter = 1;

        echo "<div class='row main-row'>";
        foreach ($front_img as $img) {
            $systemName = $img['projectName'];
            $name = preg_replace('/_/', ' ', $img['projectName']);
            echo "<div class='col-md-6' style=''>
                  <a class='link-container' href='project.php?project=" . $img['projectName'] . "'>
                  <div class='figure'>
                    <p>
                     <img class='project-image' hover-data='true' style='max-height:210px;' src='/" .
                PATH_TO_UPLOAD . "/project_" .
                $systemName.'/project_header_img/' .
                $img['projectImage'] . "'/>
                     </p>
                     <div class='figcaption'>
                     <h3 class='project-name'>".$name."</h3></a>
                     <p class='link link-tag'>";
            $countTags = 0;
            $i = count($img['creativeFields']);
            foreach ($img['creativeFields'] as $frontTags) {
                echo "<a class='link' href='tag.php?tag=".
                    $frontTags['creative_field'] . "'>" . $frontTags['creative_field'];
                if ($countTags != $i- 1) {
                    echo ', ';
                }
                echo "</a>";
                ++$countTags;
            }
            echo "</p>
                     </div>
                     </div>
                   <a/>
                  </div>";
            if ($counter%2==0) {
                echo "</div><div class='row' >";
            }
            $counter++;
        }
        echo "</div>";
        ?>
    </div>
    <div class="col-md-2">
        <div class="nav-module">
            <?php include "templates/sidebar.php"; ?>
        </div>
    </div>
</div>
<?php include_once "templates/footer.php"; ?>

