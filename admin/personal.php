<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 12.03.15
 * Time: 11:28
 */
session_start();
if (empty($_SESSION['login']) && empty($_SESSION['pass']) && $_SESSION['error_msg'] != '') {
    $_SESSION['error_msg'] = 'you have bad credentials';
    header("Location: /admin/login.php", true, 301);
} else {
}


require_once "templates/header.php";
require_once "controller/personalInfoController.php";
$form = new personalInfoController();
$formRow = $form->getSocialName();
?>
<div class="row main-wrapper">
    <div class="col-md-9 col-md-offset-1 content">
        <p class="small text-muted"><?php echo $_SESSION['greeting']; ?></p>
        <h1 style=" color: deepskyblue;">Personal Info</h1>
      <!--  <div class="container">-->
        <div class="row">
            <div class="col-md-6">
                <form action="" class="fileUpload" method="post" enctype="multipart/form-data" name="uploadHeaderImg" id="uploadHeaderImg" >
                    <!--image-->
                    <div class="row" data-toggle="tooltip" data-placement="left" title="This image will use in page header">
                        <div class="col-md-4"><label class="field">Header image</label> </div>
                        <div class="col-md-6">
                            <label class="alert-success fileUploaderInput">
                                    <span>
                                    <i class="fa fa-file-image-o" style="font-size: medium;"></i>
                                    <span class="img_path">Select image</span>
                                </span>
                                <input type="file" name="header_image" accept="image/*" style="display: none"/>
                                <input name="img" value="headerImage" type="hidden"/>
                                <span class="imageIndicator"></span>
                            </label>
                        </div>
                        <div class="col-md-2">
                            <button type="button" id="upload-header-img" class="m-btn m-btn-group blue personalBtn">
                                upload
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-4" id="msg-status"></div>
                    </div>
                </form>
                <form action="" class="fileUpload" method="post" enctype="multipart/form-data" name="uploadAboutImg" id="uploadAboutImg">
                    <div class="row" data-toggle="tooltip" data-placement="left" title="This image will use in about page">
                        <div class="col-md-4"><label class="field">About Image</label> </div>
                        <div class="col-md-6"><label class="alert-success fileUploaderInput">
                                    <span>
                                    <i class="fa fa-file-image-o" style="font-size: medium;"></i>
                                    <span class="img_path">Select image</span>
                                </span>
                                <input type="file" name="about_image" accept="image/*" style="display: none"/>
                                <input name="img" value="aboutImage" type="hidden"/>
                                <span class="imageIndicator"></span>
                            </label></div>
                        <div class="col-md-2">
                            <button type="button" id="upload-about-image" class="m-btn m-btn-group blue personalBtn">
                                upload
                            </button>
                        </div>
                    </div>
                </form>
                <form action="" class="fileUpload" method="post" enctype="multipart/form-data" name="uploadIcon" id="uploadIcon">
                    <div class="row" data-toggle="tooltip", data-placement="left" title="this image must be in .ico MIME and use as icon in browser">
                        <div class="col-md-4"><label class="field">Icon image</label> </div>
                        <div class="col-md-6"><label class="alert-success fileUploaderInput">
                                    <span>
                                    <i class="fa fa-file-image-o" style="font-size: medium;"></i>
                                    <span class="img_path">Select image</span>
                                </span>
                                <input type="file" name="favIconImage" accept="image/favicon" style="display: none"/>
                                <span class="imageIndicator"></span>
                            </label></div>
                        <div class="col-md-2">
                            <button type="button" id="upload-icon-image" class="m-btn m-btn-group blue personalBtn disabled">
                                upload
                            </button>
                        </div>
                    </div>
                </form>
                    <div class="row" data-toggle="tooltip" data-placement="left" title="This description will be visible on about page" style="position: relative; top: 10px;">
                        <div class="col-md-4"><label class="field" for="prDescription">Description</label></div>
                        <div class="col-md-8">
                            <button type="button" class="m-btn m-btn-group small blue" id="prDescription"  data-toggle="modal" data-target="#descriptionModal">add public description</button>
                        </div>
                        </div>
            </div>
            <div class="col-md-6">
                <form action="" method="post" id="uploadAboutData" style="margin-top: 0;">
                    <div class="s_links">
                      <!-- <pre> -->
                        <?php
                        //print_r($formRow);
                        foreach ($formRow as $newValue) {
                            // TODO create table view for this data remove divs
                            echo "<div class='row' style='margin-bottom: 5px;'>
                                    <div class='col-md-2' style='overflow: hidden;'>
                                      <label class='field' for='sl_" . $newValue['id'] . "'>" . $newValue['name'] . "
                                      </label>
                                    </div>
                                    <div class='col-md-6'>
                                    <!-- TODO change for string, remove input -->
                                      <p style='max-width:216px; overflow:hidden;' data-link-id='". $newValue['id']. "'>" . $newValue['link'] . "</p>
                                    </div>
                                    <div class='col-md-4'>
                                     <div class='btn-group'>
                                       <button class='m-btn m-btn-group blue updater' data-toggle='modal' data-target='#UpdateLinkModal'>
                                         <i class='fa fa-check'></i>
                                       </button>
                                       <button class='m-btn m-btn-group red deleter'>
                                         <i class='fa fa-times'></i>
                                       </button>
                                       </div>
                                    </div>
                                  </div>";
                        }
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <a href="#" data-toggle="modal" data-target="#socialLink" class="btn btn-link">+ link</a>
                            <div class="btn-group">
                                <input name="saveAbout" value="saveAbout" type="hidden"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

            <div class="modal fade" id="socialLink" tabindex="-1" role="dialog" aria-labelledby="socialLinkLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="socialLinkLabel">Add Link</h4>
                        </div>
                        <form action="" name="socialLinksForm" method="post">
                        <div class="modal-body">
                                <div class="input-group">
                                    <label for="modal-input">Name:
                                        <input type="text" name="socialName" id="modal-input"/>
                                    </label>
                                    <label for="modal-link">Link:
                                        <input type="text" name="socialLink" id="modal-link"/>
                                    </label>
                                </div>
                        </div>
                        <div class="modal-footer" style="padding-right: 30px;">
                            <input name="saveSocial" value="social" type="hidden"/>
                            <button type="button" class="m-btn m-btn-group small red" data-dismiss="modal">close</button>
                            <button type="button" id="save-social" class="m-btn m-btn-group blue">Save</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
      <!--  </div>-->
    </div>
    <div class="col-md-2 navigation">
        <div class="nav-module">
            <?php
            require_once "templates/sidebar.php";
            ?>
        </div>
    </div>
</div>
<?php
require_once "templates/descriptionModal.php";
require_once "templates/updateLink.php";
require_once "templates/footer.php";
