<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 12.03.15
 * Time: 11:28
 */
session_start();
if (empty($_SESSION['login']) && empty($_SESSION['pass']) && $_SESSION['error_msg'] != '') {
    $_SESSION['error_msg'] = 'you have bad credentials';
    header("Location: /admin/login.php", true, 301);
} else {
}
include_once "templates/header.php";

require_once "controller/uploadController.php";

$upload = new uploadController();

?>
    <div class="row main-wrapper">
        <div class="col-md-9 col-md-offset-1 content">
            <p class="small text-muted"><?php echo $_SESSION['greeting']; ?></p>
            <div class="row">
                <div class="col-md-6">
                <h1 class="left" style=" width: 300px; color: deepskyblue;">Upload project</h1>

                </div>
                <div class="col-md-6">

                    <h1 class="left" style=" width: 300px; color: deepskyblue;">Upload items</h1>
                </div>
            </div>
            <!--<div class="container">-->
            <div class="row">
                <div class="col-md-6">
                    <?php
                    $creativeFields = $upload->getCreativeFields();
                    include_once 'templates/projectForm.php';
                    ?>
                </div>
                <div class="col-md-6">
                    <form action="" id="uploadProjectItems" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-3" style="margin-bottom: 10px;">
                                <div class="btn-group">
                                    <button type="submit" id="sendProjectItems" name="saveImg" value="sendItems" class="m-btn m-btn-group blue">send</button>
                                    <button type="button" id="addInput" class="m-btn m-btn-group green">add</button>
                                    <button type="button" id="addLink" class="m-btn m-btn-group green">video link</button>
                                    <button type="reset" id="resetItems" class=" m-btn m-btn-group red">reset all</button>
                                    <input name="sendProjectItems" value="projectItems" type="hidden"/>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-3"><label class="field" for="selectProject">select project</label> </div>
                            <div class="col-md-9">
                                <select name="selectProject" multiple required="required" id="selectProject">
                                    <option value="default">----</option>
                                    <?php
                                    $list = $upload->setProjects();
                                    foreach ($list as $option) {
                                        echo 'list';
                                        $name = preg_replace('/_/', ' ', $option['projectName']);
                                        echo "<option  value='project_" .
                                            $option['projectName'] . "'>" .
                                            $name .
                                            "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row imageFiles">
                            <div class="col-md-3"></div>
                            <div class="col-md-8">
                                <label class="alert-success fileUploaderInput">
                                    <span>
                                    <i class="fa fa-file-image-o" style="font-size: large;"></i>
                                    <span class="img_path">Select image</span>
                                </span>
                                    <input type="file" name="prItemImage[]" accept="image/*" style="display: none"/>
                                    <span class="imageIndicator"></span>
                                </label>
                            </div>
                            <div class="col-md-1">
                                <button class="btn btn-sm btn-danger reset"> <i class="fa fa-remove"></i></button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-8">
                                <label class="alert-success fileUploaderInput">
                                    <span>
                                    <i class="fa fa-file-image-o" style="font-size: large;"></i>
                                    <span class="img_path">Select image</span>
                                </span>
                                    <input type="file" name="prItemImage[]" accept="image/*" style="display: none"/>
                                    <span class="imageIndicator"></span>
                                </label>
                            </div>
                            <div class="col-md-1">
                                <button class="btn btn-sm btn-danger reset"> <i class="fa fa-remove"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--</div>-->
        </div>
        <div class="col-md-2 navigation">
            <div class="nav-module">
                <?php
                include_once "templates/sidebar.php";
                ?>
            </div>
        </div>
    </div>
<?php
//include_once 'templates/descriptionModal.php';
include_once 'templates/footer.php';
?>