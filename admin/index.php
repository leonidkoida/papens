<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 01.05.15
 * Time: 23:39
 */

session_start();// create session controller something like this require 'Session.php' $session = new Session()
//$session ->start();
if ((empty($_SESSION['login']) and empty($_SESSION['pass'])) or $_SESSION['error_msg'] != '') {
    $_SESSION['error_msg'] = 'you have bad credentials';
    header("Location: /admin/login.php", true, 301);
} else {
}
include_once 'templates/header.php';
require_once 'controller/mainController.php';
$Control = new mainController();
$emails = $Control->getEmails();
?>
<div class="row main-wrapper">
    <div class="col-md-9 col-md-offset-1 content">
        <p class="small text-muted"><?php echo $_SESSION['greeting']; ?></p>
            <h1 class="left" style=" color:deepskyblue">Look & Feel</h1>
            <div class="row">
                <!--nav tabs-->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active" role="presentation">
                        <a href="#users" aria-controls="users" role="tab" data-toggle="tab">Users</a>
                    </li>
                    <li role="presentation">
                        <a href="#tags" aria-controls="tags" role="tab" data-toggle="tab">Tags</a>
                    </li>
                    <li role="presentation">
                        <a href="#system-info" aria-controls="sistem-info" role="tab" data-toggle="tab">System Info</a>
                    </li>
                    <li role="presentation">
                        <a href="#graphs" aria-controls="graphs" role="tab" data-toggle="tab">Graphs</a>
                    </li>
                    <li role="presentation">
                        <a href="#emails" aria-controls="emails" role="tab" data-toggle="tab">Emails</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="users" role="tabpanel">
                        <div class="col-md-12">
                            <form action="" method="post" class="fileUploadForm" id="manageUsers">
                                <div class="buttons-field text-right">
                                    <button type="button" class="m-btn small m-btn-group blue" data-toggle="modal" data-target="#usersModal" data-whatever="users">add</button>
                                </div>
                                <table class="table table-striped table-condensed table-bordered">
                                    <tr>
                                        <th>login</th>
                                        <th>email</th>
                                        <th style="width: 100px;" class="text-center"><i class="fa fa-gear"></i></th>
                                    </tr>
                                    <tbody>
                                    <?php
                                    $users = $Control->getAllUsers();
                                    foreach ($users as $user) {
                                        echo " <tr>
                                <td>{$user['login']}</td>
                                <td>{$user['email']}</td>";
                                        echo "
                                <td>
                                    <button class='m-btn m-btn-group small blue'data-user-id='{$user['id']}' data-toggle='modal'  data-target='#updateUserModal'><i class='fa fa-pencil-square'></i></button>
                                    <button class='m-btn m-btn-group small red delete-user' data-user-delete='{$user['id']}'><i class='fa fa-trash'></i></button>
                                </td>
                            </tr>";
                                    }
                                    ?>
                                    <tr>
                                    </tbody>
                                </table>
                            </form>

                        </div>
                    </div>
                    <div class="tab-pane" id="tags" role="tabpanel">
                        <!--here will be form to upload o change creative tags-->
                        <div class="col-md-12">
                            <form action="" method="post" class="fileUploadForm" id="manageCreativeFields">
                                <table class="table table-striped table-condensed table-bordered">
                                    <div class="buttons-field text-right">
                                        <button type="button" class="m-btn small m-btn-group blue" data-toggle="modal" data-target="#exampleModal" data-whatever="tags">add</button>
                                    </div>
                                    <tr>
                                        <th>name</th>
                                        <th style="width: 100px;" class="text-center"><i class="fa fa-gear"></i></th>
                                    </tr>
                                    <tbody>
                                    <tr>
                                    </tr>
                                    <?php
                                    $tags = $Control->getTags();
                                    foreach ($tags as $tagname) {
                                        echo " <tr id='" . $tagname['id'] . "'>
                                         <td>" . $tagname['creative_field'] . "</td>
                                         <td>
                                         <button type='button' class='m-btn m-btn-group small blue update' data-toggle='modal' data-target='#updateTagModal' data-update='" . $tagname['id'] . "'><i class='fa fa-pencil-square'></i></button>
                                         <button type='button' class='m-btn m-btn-group small  red remove' data-toggle='modal' data-target='#confirmModal' data-remove='" . $tagname['id'] . "'><i class='fa fa-trash'></i></button>
                                        </td>
                                      </tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </form>

                        </div>
                    </div>
                    <div class="tab-pane" id="system-info" role="tabpanel">
                        <div class="col-md-12">
                            <form action="" method="post" id="lookAndFeelForm">
                                <table class="table table-striped table-condensed table-bordered">
                                    <thead>
                                    <tr>
                                        <th style="min-width:153px;">System text(footer and meta)</th>
                                        <th><i class="fa fa-gear"></i></th>
                                        <th style="min-width: 135px">About page</th>
                                        <th><i class="fa fa-gear"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $systemInfo = $Control->getAllSystemInfo();
                                    echo "<tr>";
                                    $counter = 1;
                                    foreach ($systemInfo as $item) {
                                        echo "<td title='" . $item['system_name'] . "'>{$item['name']}</td>
                              <td>
                              <button class='m-btn m-btn-group small green footerData' data-toggle='modal' data-target='#systemInfoModal'  data-field-id='{$item["id"]}' data-field-name='{$item["system_name"]}'>
                              <i class='fa fa-pencil-square'></i>
                              </button>
                              </button> </td>";
                                        if ($counter%2 == 0) {
                                            echo "</tr><tr>";
                                        }
                                        $counter++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="graphs" role="tabpanel">
                        <div class="col-md-12" >here will be graphs
                        </div>
                    </div>
                    <div class="tab-pane" id="emails" role="tabpanel">
                        <table class="table table-striped table-hover">
                        <?php foreach($emails as $email):?>
                            <tr>
                                <td><?= $email['user_name']?></td>
                                <td><a href="mailto:<?= $email['user_email']?>"><?= $email['user_email']?></a></td>
                                <td><?= $email['user_text']?></td>
                            </tr>
                        <?php endforeach;?>
                        </table>
                    </div>
                </div>
            </div>
<!--        </div>-->
    </div>
    <div class="col-md-2 navigation">
        <div class="nav-module">
            <?php
            include_once 'templates/sidebar.php';
            ?>
        </div>
    </div>
</div>
<?php
include_once 'templates/confirmModal.php';
include_once 'templates/addUserModal.php';
include_once 'templates/addTagModal.php';
include_once 'templates/modals/updateTagModal.php';
include_once 'templates/updateUserModal.php';
include_once 'templates/systemInfoModal.php';
include_once "templates/footer.php";