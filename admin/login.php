<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 12.03.15
 * Time: 11:28
 */
session_start();
require_once 'controller/mainController.php';

$checkPassword = new mainController();
$sitename = $checkPassword->getSiteName();
preg_match('/\w+/', $sitename['name'], $matches);
$sitename = $matches[0];
if ($_POST) {
    $login = trim(htmlspecialchars($_POST['login']));
    $pass = md5(trim(htmlspecialchars($_POST['password'])));
    $confirm =$checkPassword->checkUserCredentials($login, $pass);

    $confirmLogin = $confirm[0][0];
    $confirmPass = $confirm[0][1];

    if ($login == $confirmLogin && $pass == $confirmPass) {
        $_SESSION['login'] = $confirmLogin;
        $_SESSION['pass'] = $confirmPass;
        $_SESSION['greeting'] = $confirm[1];
        $_SESSION['error_msg'] = '';

        header("Location: /admin/index.php");
    } else {
        $_SESSION['error_msg'] = 'you have bad credentials';
        header("HTTP/1.1 401 Unauthorized");
    }
} elseif (!empty($_SESSION)) {
}
include_once "templates/header.php";
?>
<div class="row main-wrapper">
    <div class="col-md-6 col-md-offset-3">
            <form class="form-signin" method="post">
                <h2 class="form-signin-heading" style="text-transform: capitalize"><?php echo $sitename; ?></h2>
<?php if (!empty($_SESSION['error_msg'])) :?>
                <p class="text-danger">
                    <?php echo $_SESSION['error_msg'];
endif;
                    ?>
                </p>
                <div class="row row-form">
                    <div class="col-md-3">
                        <label for="login" style="font-weight: 500">
                            login:
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="login" id="login" placeholder="login" tabindex="1"/>
                    </div>
                </div>
                <div class="row row-form">
                   <div class="col-md-3">
                       <label for="password" style="font-weight: 500">
                           Password:
                       </label>
                   </div>
                    <div class="col-md-6">
                        <input type="password" id="password" name="password" placeholder="password" tabindex="2"/>
                    </div>
                </div>
                <div class="row row-form">
                    <div class="form-group">
                        <div class="col-md-3"></div>
                        <div class="col-md-6" style="padding-right: 26px;">
                            <a class="link" href="#" tabindex="4">forgot password?</a>
                            <button class="m-btn m-btn-group small blue " style="float: right;" type="submit" tabindex="3">ok</button>
                        </div>
                    </div>
                </div>
            </form>
    </div>
</div>
