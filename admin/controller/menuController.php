<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 07.04.15
 * Time: 1:06
 */
require_once 'ConnectController.php';

class Menu extends ConnectController {

    public $menu = [];

    public function menu()
    {
        $result = $this->setMysqli()->query('SELECT * FROM admin_menu ORDER BY id DESC ');
        while ($row = $result->fetch_assoc()) {
           $this->menu[] = $row;
        }
        $result->close();
      return $this->menu;
    }
}