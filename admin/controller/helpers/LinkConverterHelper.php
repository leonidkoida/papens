<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 25.07.15
 * Time: 12:46
 */

/**
 * Class LinkConverterHelper
 */
class LinkConverterHelper
{
    private $patternForYoutube = "/(\w{4,5}):\/\/(w{3}\.\w+\.\w{3})(\/\w+\?v=)(\w+)/";

    private $patternForVimeo = "/(\w{5}):\/\/(\w+\.\w{3})(\/\w+\/\w+\/)(\w+)/";

    private $checkChannel = "/(\w{5}):\/\/w*\.?(\w+)/";

    public static $YOUTUBE = 'youtube';

    public static $VIMEO = 'vimeo';

    public $youtubeFormatted = "https://www.youtube.com/embed/";

    public $vimeoFormatted = "https://player.vimeo.com/video/";

    public function getPatternForYoutube()
    {
        return $this->patternForYoutube;
    }

    public function getPatternForVimeo()
    {
        return $this->patternForVimeo;
    }

    public function getCheckChannel()
    {
        return $this->checkChannel;
    }


    public function detectCh(array $link)
    {
        $linkVimeo = '';
        $linkYoutube = '';

        if (is_array($link)) {
            $numLinks = count($link);
            $vimeo = [];
            $youtube = [];
            for ($i=0; $i<$numLinks; $i++) {
                preg_match($this->checkChannel, $link[$i], $matches);
                if (self::$VIMEO === $matches[2]) {
                    array_push($vimeo, $link[$i]);
                }
                if (self::$YOUTUBE === $matches[2]) {
                    array_push($youtube, $link[$i]);
                }
            }
            $linkYoutube = $this->createLinkYoutube($youtube);
            $linkVimeo = $this->createLinkVimeo($vimeo);
        }
        return ['youtube' => $linkYoutube, 'vimeo' => $linkVimeo];
    }

    public function createLinkYoutube(array $youtube)
    {
        $linksYoutubeFormatted = [];
        $countArray = count($youtube);

        for ($i=0; $i<$countArray; $i++) {
            preg_match($this->patternForYoutube, $youtube[$i], $matches);
            $link = $this->youtubeFormatted . $matches[4];
            array_push($linksYoutubeFormatted, $link);
        }
        return $linksYoutubeFormatted;
    }

    public function createLinkVimeo(array $vimeo)
    {
        $linksVimeoFormatted = [];
        $countArray = count($vimeo);

        for ($i=0; $i<$countArray; $i++) {
            preg_match($this->patternForVimeo, $vimeo[$i], $matches);
            $link = $this->vimeoFormatted . $matches[4];
            array_push($linksVimeoFormatted, $link);
        }
        return $linksVimeoFormatted;
    }
}
