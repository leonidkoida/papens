<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 28.03.15
 * Time: 20:37
 */

/**
 * Class CheckImg
 */
class CheckImg extends Exception {

    private $max_size = 1024;

    private $number_of_img = 10;

    private $mime =['jpg','jpeg', 'gif', 'png'];

    private $error = [];


    /**
     * @return int
     */
    public function getMaxSize()
    {
        return $this->max_size;
    }

    /**
     * @return int
     */
    public function numberOfImg()
    {
        return $this->number_of_img;
    }

    /**
     * @return array
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * @return array
     */
    public function getError()
    {
        return $this->error;
    }


    /**
     * @param $file_size
     * @return array
     */
    public function checkSize($file_size)
    {
        if ($file_size > $this->max_size) {
            $this->error[] = 'to big file';
        }
        return $this->error;
    }

    /**
     * @param $file_mime
     * @return array
     */
    public function checkMime($file_mime)
    {
        if (!in_array($file_mime, $this->mime)) {
            $this->error[] = 'wrong extension';
        }
        return $this->error;
    }

    /**
     * @param $file_number
     * @return array|string
     */
    public function checkNumberOfImg($file_number)
    {
        if (!count($file_number) == $this->number_of_img) {
            return $this->error[] = 'to much files to upload';
        }
        return $this->error;
    }
}
