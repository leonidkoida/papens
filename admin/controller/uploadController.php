<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 15.03.15
 * Time: 0:18
 */
require_once 'ConnectController.php';
//require_once 'helpers/CheckImg.php';
require_once 'helpers/LinkConverterHelper.php';
/**
 * Class uploadController
 */
class uploadController extends ConnectController
{
    private $max_file_size = 1024;

    private $max_num_files = 10;

    private $mime_type = ['jpg', 'png', 'jpeg'];

    private $upload_project_dir = 'Assets/img/upload';

    private $projectList;

    public $msg = [];

    public $row;

    public function getMaxSize()
    {
        return $this->max_file_size;
    }

    public function getNumFiles()
    {
        return $this->max_num_files;
    }

    public function getMimeType()
    {
        return $this->mime_type;
    }

    public function getUploadDir()
    {
        return $this->upload_project_dir;
    }

    public function getProjectList()
    {
        return $this->projectList;
    }

    public function setProjectName($projectList)
    {
        $this->projectList = $projectList;
        return $projectList;
    }

    /**
     * @param $files
     * @param $post
     * @return string
     * @throws Exception
     */
    public function uploadProject($files, $post)
    {
        $file_project_img       = $files['uploadProjectImage']['name'];
        //$file_project_type      = $files['uploadProjectImage']['type'];// TODO create check for good mime type
        $file_project_temp_name = $files['uploadProjectImage']['tmp_name'];
        $file_project_error     = $files['uploadProjectImage']['error'];
        //$file_project_size      = $files['uploadProjectImage']['size'];// TODO create check for good size

        $project_name    = trim(htmlspecialchars($post['prName']));
        $project_name = preg_replace('/\s/', '_', $project_name);

        $project_client  = trim(htmlspecialchars($post['prClient']));
        $creative_fields = json_decode($post['creative']);
        $project_date  = $post['prDate'];
        $project_date = date('Y-m-d H:i:s', strtotime($project_date));
        $project_description = $post['projectDescription'];
//todo  move tis to helpers/CheckImg
        if ($file_project_error != UPLOAD_ERR_OK) {
            switch ($file_project_error) {
                case UPLOAD_ERR_NO_FILE:
                    $this->msg[] = "file not found, try to upload again";
                    break;
                case UPLOAD_ERR_INI_SIZE:
                    $this->msg[] = "you try to upload too big file";
                    break;
                case UPLOAD_ERR_FORM_SIZE:
                    $this->msg = "to big file";
                    break;
                case UPLOAD_ERR_CANT_WRITE:
                    $this->msg[] = "cant write file to hd try one more.";
                    break;
                default:
                    $this->msg[] = 'something went wrong, try again';
            }
        } else {
            if ($post['saveProject']== 'projectSave') {
                $check_project = $this->setMysqli()->query("SELECT projectName
                                                            FROM t_Project
                                                            WHERE projectName = '$project_name'");
                if ($check_project->num_rows>0) {
                    $this->msg[] = "this project already exist, please navigate to <a href='" .
                                    ROOT."admin/manage.php'>Manage projects</a> page to change it";
                } else {
                    /*folder for main image*/
                    mkdir(PATH_TO_PROJECT.$project_name.'/project_header_img', 0777, true);
                    /*folder for project items*/
                    mkdir(PATH_TO_PROJECT.$project_name.'/items', 0777, true);
                    // TODO on iterator some problems in logs file
                    $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(PATH_TO_PROJECT.$project_name));
                    foreach ($iterator as $item) {
                        chmod($item, 0777);
                    }

                    $img_url = PATH_TO_PROJECT.$project_name.'/project_header_img/';
                    move_uploaded_file($file_project_temp_name, "$img_url/$file_project_img");

                    $this->setMysqli()
                      ->query("
                      INSERT INTO t_Project
                      (projectName, projectImage, pub_date,client, projectDescription)
                      VALUES
                      ('$project_name','$file_project_img','$project_date' ,'$project_client', '$project_description')");

                    $id = $this->setMysqli()->query("SELECT id FROM t_Project WHERE projectName = '$project_name'");
                    if ($id->num_rows > 0) {
                        $prId='';
                        while ($row = $id->fetch_assoc()) {
                            $prId = $row['id'];
                        }
                        if ($prId > 0) {
                            foreach ($creative_fields as $creativeField) {
                                $this->setMysqli()->query("INSERT INTO index_creative(creative_field_id, project_id)
                                                           VALUES ($creativeField, $prId)");
                            }
                        }
                    } else {
                        $this->msg[] = 'this is unknown project';
                    }
                    $this->setMysqli()->close();
                    $this->msg[] = "files and date is uploaded <br>";

                     return $this->projectList = $project_name;
                }
            }
        }
        return $this->msg;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getCreativeFields()
    {
        $data = [];
        $result = $this->setMysqli()->query("SELECT * FROM Creative_Fields ORDER BY id ASC");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        } else {
            $this->msg[] = 'no items in database';
        }
        return $data;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function setProjects()
    {
        $my_result = $this->setMysqli()->query("SELECT * FROM t_Project");
        $my_array = [];
        while ($row = $my_result->fetch_assoc()) {
            $my_array[] = $row;
        }
        $my_result->close();
        return $my_array;
    }

    /**
     * @param $files
     * @param $post
     * @return string
     */
    public function setProjectItems($files, $post)
    {
        if ($post['selectProject'] == 'default') {
            $this->msg[] = 'you forget select a existing project.';
            return $this->msg;
        }
        $item_name = $files['prItemImage']['name'];
        $item_tmp_name = $files['prItemImage']['tmp_name'];
        //$item_error = $files['prItemImage']['error'];
        //$item_size = $files['prItemImage']['size'];
        //$item_type = $files['prItemImage']['type'];

        $project_name = trim(htmlspecialchars($post['selectProject']));
        $project_name = substr($project_name, 8);

        $img_url = PATH_TO_PROJECT.$project_name.'/items/';

        if (!file_exists($img_url)) {
            // TODO do something with this message
            $this->msg[] = "Directory {$img_url} not exist try to create it again.";
            //$this->msg[] = "directory {$img_url} <br> already exists<br> want to override it?";
        } /*else {
            $this->msg[] = "Directory {$img_url} not exist try to create it again.";
        }*/
        $get_project_id = $this->setMysqli()->query("SELECT id, projectName
                                                     FROM t_Project
                                                     WHERE projectName = '$project_name'");

        if ($get_project_id->num_rows == false) {
            $this->msg[] ="not found project name";
        } else {
            $id = '';
            $count = count($files['prItemImage']['name']);

            while ($prId = $get_project_id->fetch_assoc()) {
                $id = $prId['id'];
                for ($f_num = 0; $f_num<$count; $f_num++) {
                    if (empty($files['prItemImage']['name'][$f_num])) {
                        $this->msg[] = "empty files";
                        continue;
                    } else {
                        $item_path = $item_name[$f_num];
                        move_uploaded_file($item_tmp_name[$f_num], "$img_url"."$item_name[$f_num]");
                        $this->setMysqli()->query("INSERT INTO ProjectItems( image_url,project_id)
                                                   VALUES ('$item_path' ,'$id')");
                    }
                }

                $this->msg[] = "files is uploaded successfully.";
            }
            $get_project_id->close();

            $query = $this->setMysqli();
            if ($post['link']) {
                $result = '';
                $link = $post['link'];


                $linkFormatted = new LinkConverterHelper();
                $src = $linkFormatted->detectCh($link);
                $srcYoutube = $src['youtube'];
                $srcVimeo = $src['vimeo'];

                if (count($srcYoutube) > 0) {
                    for ($i=0; $i<count($srcYoutube); $i++) {
                         $result = $query
                                   ->query("INSERT INTO Video_links(link, project_id)
                                            VALUES ('$srcYoutube[$i]', $id)");
                    }
                }
                if (count($srcVimeo) > 0) {
                    print_r($srcVimeo);
                    for ($i=0; $i<count($srcVimeo); $i++) {
                        $result = $query
                            ->query("INSERT INTO Video_links(link, project_id)
                                            VALUES ('$srcVimeo[$i]', $id)");
                    }
                }
                if ($result) {
                    $this->msg[] = 'links a loaded successfully';
                }
            } else {
                $this->msg[] = 'links is empty';
            }
            $query->close();
        }

        return $this->msg;
    }
}
