<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 26.03.15
 * Time: 1:32
 */

require_once 'ConnectController.php';

class personalInfoController extends ConnectController
{

    public $data = [];


    /**
     * @param $files
     */
    public function addIconImage($files)
    {
        $this->uploadSystemImages($files, 'header_image', 'logo', 'header_image_url');
    }

    /**
     * @param $files
     */
    public function addAboutImage($files)
    {
        $this->uploadSystemImages($files, 'about_image', 'imgAbout', 'about_image_url');
    }

    public function addFavIconImage($files)
    {
        $this->uploadSystemImages($files, 'favIconImage', 'logo/icons', 'icon_image_url');
    }


    /**
     * @param $files
     * @param $array_name
     * @param $path
     * @param $field
     *
     * @return array
     * @throws Exception
     */
    private function uploadSystemImages($files, $array_name, $path, $field)
    {

        /*TODO create sanitise filter for image*/
        $name = $files["$array_name"]['name'];
        $type = $files["$array_name"]['type'];
        $temp_name = $files["$array_name"]['tmp_name'];
        $error = $files["$array_name"]['error'];
        $size = $files["$array_name"]['size'];

        if (file_exists(PATH_TO_SYSTEM_IMG . "/" . $path)) {
            $imgPath = PATH_TO_SYSTEM_IMG . "/" . $path;

            $check = $this->setMysqli()->query("SELECT $field  FROM PersonalInfo");

            $movedFile = move_uploaded_file($temp_name, "$imgPath/$name");
            if ($movedFile) {
                if ($check->num_rows > 0) {
                    $this->setMysqli()->query("UPDATE PersonalInfo SET $field='$name' ");
                    $this->data[] = 'image was loaded successfully';
                } else {
                    $this->setMysqli()->query("INSERT INTO PersonalInfo ($field) VALUES ('$name')");
                    $this->data[] = 'image was loaded successfully';
                }
            } else {
                http_response_code(411);
                $this->data[] = 'image not uploaded';
            }
        } else {
            http_response_code(404);
            $this->data[] = 'directory does not exist';
        }
        return $this->data;
    }

    /**
     * Set social links to database
     *
     * @param $post
     *
     * @return array
     *
     * @throws Exception
     */
    public function setSocialLinkName($post)
    {
        if (isset($post)) {
            $linkName = trim(htmlspecialchars($post['socialName']));
            $link = trim(htmlspecialchars($post['socialLink']));
            if (!empty($linkName) || !empty($link)) {
                 $result = $this->setMysqli()->query("INSERT INTO social_links(name, link)
                                                      VALUES ('$linkName', '$link')");
                if ($result) {
                    $this->data['success'] = 'link for  "' . $linkName . '/'. $link . '" successfully created';
                    $this->data['name'] = $linkName;
                    $this->data['link'] = $link;
                } else {
                    http_response_code(411);
                    $this->data['error'] = 'please try again set the link';
                }
            } else {
                http_response_code(411);
                $this->data['error'] = 'data is empty, please fill all required fields.';
            }
        }
        return $this->data;
    }

    /**
     * @param $post
     * @return array
     * @throws Exception
     */
    public function updateSocialLink($post)
    {
        //todo create some check to correct links
        //TODO create correct preg_match function to find unmatched data

        $id = trim($post['linkId']);
        $linkName = trim($post['linkName']);
        $linkAddress = trim($post['linkAddress']);

        $getLink = $this->setMysqli()->query("SELECT * FROM social_links WHERE id='$id'");

        if ($getLink->num_rows>0) {
            $updateSocialLink = $this->setMysqli();
            while ($row = $getLink->fetch_assoc()) {
                    $updateSocialLink->query("UPDATE social_links SET name='$linkName' WHERE id='$id'");
                    $this->data['name'] = $linkName;

                    $updateSocialLink->query("UPDATE social_links SET link='$linkAddress'  WHERE id='$id'");
                    $this->data['address'] = $linkAddress;
            }
            $updateSocialLink->close();
        } else {
            $this->data[] = 'no such social link';
        }
        return $this->data;
    }

    public function deleteSocialLink($id)
    {
        $check = $this->setMysqli()->query("SELECT id FROM social_links WHERE id='$id'");
        if ($check == 0) {
            $this->data[] = 'this link does not exist';
        } else {
            $remove = $this->setMysqli()->query("DELETE FROM social_links WHERE id='$id' LIMIT 1");
            if ($remove) {
                $this->data['success'] = 'link removed successfully';
            }
        }
        return $this->data;
    }

    /**
     * @param $desc
     * @return array
     * @throws Exception
     */
    public function setAboutDescription($desc)
    {
        $connect = $this->setMysqli();
        if (!empty($desc)) {
            $prepare_update = $connect->query("SELECT description FROM PersonalInfo");
            if ($prepare_update->num_rows != 0) {
                $result = $connect->query("UPDATE PersonalInfo SET description='$desc'");
                $message = 'description was updated';

            } else {
                $result = $connect->query("INSERT INTO PersonalInfo(description) VALUES ('$desc')");
                $message = 'description was inserted';
            }
            if ($result) {
                $this->data['success'] = $message;
            } else {
                http_response_code(415);
                $this->data['error'] = 'something wrong, please try ones more';
            }

        } else {
            http_response_code(404);
            $this->data['error'] = 'data is empty, try once more';
        }
        $connect->close();
        return $this->data;
    }

    /*public function setContactEmail($email)
    {
        $pattern = '/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i';
        $checkEmail = preg_match($pattern, $email);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) || !$checkEmail ) {
            $this->data['error'] = 'invalid email address';
        } else {
            $prepare_update = $this->setMysqli()->query("SELECT email FROM PersonalInfo");
        }

    }*/

    /**
     * @return array
     * @throws Exception
     */
    public function getSocialName()
    {
        $result = $this->setMysqli()->query("SELECT * FROM social_links");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $this->data[] = $row;
            }
        }
        return $this->data;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getAboutDescription()
    {
        $result = $this->setMysqli()->query("SELECT description FROM PersonalInfo LIMIT 1");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $this->data = $row;
            }
        } else {
            $this->data[] = 'no description';
        }
        return $this->data;
    }

} 