<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 20.04.15
 * Time: 16:40
 */

require_once 'manageController.php';
require_once 'lookAndFeelController.php';
require_once 'mainController.php';
require_once 'menuController.php';
require_once 'personalInfoController.php';
require_once 'uploadController.php';

if ($_POST) {
    $project = new manageController();
    if (isset($_POST['status'])) {
        $id = $_POST['status'][0]['id']*1;
        $status = $_POST['status'][0]['state'];
        if (!is_numeric($status)) {
            echo 'please enter a valid value';
        } else {
            $project->changeStatus($status, $id);
        }
    } elseif (isset($_POST['priority'])) {
        $id = $_POST['priority'][0]['id']*1;
        $priority = $_POST['priority'][0]['priority']*1;
        if (!is_numeric($id) || !is_numeric($priority)) {
            //todo throw Exception
            echo 'please enter a valid value';
        } else {
            $project->changePriority($id, $priority);
        }
    } elseif (isset($_POST['project_delete'])) {
        $id = $_POST['project_delete'][0]['id']*1;
        $project->deleteProject($id);
        $data = $project->msg;
        echo json_encode(['data'=>$data]);

    } elseif (isset($_POST['fieldName'])) {/*personal info behavior*/
        $field = new lookAndFeelController();
        $rowName = trim(htmlspecialchars($_POST['fieldName'][0]['fieldName']));
        $rowValue = trim(htmlspecialchars($_POST['fieldName'][0]['fieldValue']));
        $field->setData($rowName, $rowValue);

    } elseif (isset($_POST['img']) && $_POST['img'] == 'headerImage') {
        if (isset($_FILES) && $_FILES['header_image']) {
            $loadIcon = new personalInfoController();
            $loadIcon->addIconImage($_FILES);
            echo json_encode(['data'=>$loadIcon->data]);

        } else {
            http_response_code(411);
            echo json_encode(['data'=>['error'=>'you send an empty data']]);
        }
    } elseif (isset($_POST['img']) && $_POST['img'] == 'aboutImage') {
        if (isset($_FILES) && $_FILES['about_image']['error'] != UPLOAD_ERR_NO_FILE) {
            $loadAboutImg = new personalInfoController();
            $loadAboutImg->addAboutImage($_FILES);
            echo json_encode(['data'=>$loadAboutImg->data]);
        } else {
            http_response_code(411);
            echo json_encode(['data'=>'you send an empty data']);
        }
    } elseif (isset($_POST['img']) && $_POST['img'] == 'iconImage') { //TODO finish this functionality
        if (isset($_FILES) && $_FILES['prItemImage']['error'] !== UPLOAD_ERR_NO_FILE) {
            $loadFavIconImg = new personalInfoController();
            $loadFavIconImg->addFavIconImage($_FILES);
            echo json_encode(['data' => $loadFavIconImg->data]);
        } else {
            http_response_code(411);
            echo json_encode(['data'=>'you send an empty data']);
        }
    } elseif (isset($_POST['saveSocial'])) {/*** social links ***/
        $personalName = new personalInfoController();
        $personalName->setSocialLinkName($_POST);
        echo json_encode(['data'=>$personalName->data]);

    } elseif (isset($_POST['linkUpdateControl']) && $_POST['linkUpdateControl'] === 'link') {
        $updateLink = new personalInfoController();
        $updateLink->updateSocialLink($_POST);
        echo json_encode(['data'=>$updateLink->data]);
    } elseif (isset($_POST['removeLink'])) {
        $linkId = $_POST['removeLink'];
        if (!is_numeric($linkId)) {
            http_response_code(404);
            return json_encode(['data'=>'must be a number']);
        } else {
            $removeLink = new personalInfoController();
            $removeLink->deleteSocialLink($linkId);
            echo json_encode(['data' => $removeLink->data]);
        }
    } elseif (isset($_POST['aboutDescription'])) {
        $setDescription = new personalInfoController();
        $description = $_POST['data'];
        $setDescription->setAboutDescription($description);
        echo json_encode(['data'=>$setDescription->data]);

    } elseif (isset($_POST['tagControl']) && $_POST['tagControl']=='TAGS') {/* create tags*/
        $tags = new mainController();
        $tag = $_POST['newTag'];
        $tags->addTags($tag);
        $data = $tags->data;
        echo json_encode(['data'=>$data]);

    } elseif (isset($_POST['tagUpdateControl'])) { /* update tags */
        $updateTag = new mainController();
        $updateTag->updateTags($_POST);
        echo json_encode($updateTag->data);

    } elseif (isset($_POST['deleteTag'])) {/*delete tags*/
        $tags = new mainController();
        $removeTag = $_POST['deleteTag'];
        $tags->deleteTags($removeTag);
        $data = $tags->data;
        echo json_encode(['data'=>$data]);

    } elseif (isset($_POST['usersControl'])) {/*manage users*/
        $users = new mainController();
        $userData = $_POST;
        $users->addUser($userData);
        $data = $users->data;
        echo json_encode($data);

    } elseif (isset($_POST["usersUpdateControl"])) {
        $users = new mainController();
            $users->updateUser($_POST);
        $data = $users->data;
        echo json_encode(['data'=>$data]);

    } elseif (isset($_POST['deleteUser'])) {
        $userId = trim(htmlspecialchars($_POST['deleteUser']));
        $user = new mainController();
        if (!is_numeric($userId)) {
            http_response_code(404);
            echo json_encode(['data'=>'id must be a digit']);
            return;
        } else {
            $user->deleteUser($userId);
            echo json_encode($user->data);
        }
    } elseif (isset($_POST['systemInfoUpdateControl'])) {
        $updateSystemInfo  = new mainController();
        $updateSystemInfo->updateSystemInfo($_POST);
        echo json_encode(['data'=>$updateSystemInfo->data]);

    } elseif (isset($_POST['saveProject']) && $_POST['saveProject'] === 'projectSave') {
    /*manage uploading project and project items*/
        $upload = new uploadController();
        $upload->uploadProject($_FILES, $_POST);
        $data = $upload->setProjects();
        echo json_encode(['data'=> $data]);

    } elseif (isset($_POST['sendProjectItems']) && $_POST['sendProjectItems'] === 'projectItems') {
        $uploadItems = new uploadController();
        $uploadItems->setProjectItems($_FILES, $_POST);
        $data = $uploadItems->msg;
        echo json_encode(['data'=> $data]);

    } elseif (isset($_POST['updateProject'])) { /* update project */
        $updateProject = new manageController();
        $updateProject->updateProject($_FILES, $_POST);
        echo json_encode(['data'=>$updateProject->msg]);

    } elseif (isset($_POST['deleteTagFromProject'])) {
        $deleteCreativeFromProject = new manageController();
        $deleteCreativeFromProject->deleteCreativeField($_POST);
        $data = $deleteCreativeFromProject->msg;
        echo json_encode(['data'=>$data]);

    } elseif ($_POST['itemPriority']) {/*manage project items*/
        $id = trim(htmlspecialchars($_POST['itemPriority']['id']));
        $dataSrc = trim(htmlspecialchars($_POST['itemPriority']['data']));
        $priority = trim(htmlspecialchars($_POST['itemPriority']['priority']));
        if (!is_numeric($id) || !is_numeric($priority)) {
            http_response_code(404);
            echo json_encode(['data'=>'values must be a numbers only']);
        } else {
            $updateItems = new manageController();
            $updateItems->updateProjectItemPriority($id, $dataSrc, $priority);
            $data = $updateItems->msg;
            echo json_encode(['data'=>$data]);
        }
    } elseif ($_POST['itemStatus']) {
        $id = trim(htmlspecialchars($_POST['itemStatus']['id']));
        $dataSrc = trim(htmlspecialchars($_POST['itemStatus']['data']));
        $status = trim(htmlspecialchars($_POST['itemStatus']['status']));
        if (!is_numeric($id) || !is_numeric($status)) {
            http_response_code(404);
            echo json_encode(['data'=>'values must be a numbers only']);
            return;
        } else {
            $updateItemsStatus = new manageController();
            $updateItemsStatus->updateProjectItemStatus($id, $dataSrc, $status);
            $data = $updateItemsStatus->msg;
            echo json_encode(['data'=>$data]);
        }
    } elseif (isset($_POST['addItemDescription'])) {
            $updateDescription = new manageController();
            $updateDescription->updateItemDescription($_POST);
            $data = $updateDescription->msg;
            echo json_encode($data);

    } elseif ($_POST['itemDelete']) {/*delete project items*/
        $id = trim(htmlspecialchars($_POST['itemDelete']['id']));
        $dataSrc = trim(htmlspecialchars($_POST['itemDelete']['data']));
        if (!is_numeric($id)) {
            http_response_code(404);
            echo json_encode(['data'=>'values must be numbers only']);
            return;
        } else {
            $deleteItem = new manageController();
            $deleteItem->deleteItem($id, $dataSrc);
            $data = $deleteItem->msg;
            echo json_encode($data);
        }
    }
}

if ($_GET) {
    if ($_GET['getProjects']) {
        $getProjects = new uploadController();
        $data = $getProjects->setProjects();
        echo json_encode(['data'=>$data]);

    } elseif ($_GET['updateProject']) {
        $projectId = trim(htmlspecialchars($_GET['id']));
        if (!is_numeric($projectId)) {
            http_response_code(404);
            echo json_encode(['data'=>'id must be an integer.']);
            return;
        }
        $getProject = new manageController();
        $data = $getProject->getProject($projectId);
        echo json_encode(['data'=>$data]);

    } elseif ($_GET['updateItems'] && $_GET['updateItems'] == 'itemsUpdate') {
        $projectId = trim(htmlspecialchars($_GET['id']));
        if (!is_numeric($projectId)) {
            http_response_code(404);
            echo json_encode(['data'=>'id must be an integer']);
            return;
        }
            $getProjectItems = new manageController();
            $data = $getProjectItems->getProjectItems($projectId);
            echo json_encode(['data'=>$data]);

    } elseif ($_GET['updateItemDescription']) {
        $id  = trim(htmlspecialchars($_GET['updateItemDescription']));
        $src = trim(htmlspecialchars($_GET['src']));
        $items = new manageController();
        $itemDescription = $items->getItemsDescription($id, $src);
        echo json_encode($itemDescription);

    } elseif ($_GET['description'] == 'about') {
        $about = new personalInfoController();
        $description = $about->getAboutdescription();
        echo json_encode(['data'=>$description]);
    }
}
