<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 12.03.15
 * Time: 11:34
 */

require_once 'ConnectController.php';

class lookAndFeelController extends ConnectController{

    public $errors;

    public function setData($row, $value)
    {
        switch ($row) {
            case 'footer_disclaimer':
                $this->setValue($row, $value);
                break;
            case 'sitename':
                $this->setValue($row, $value);
                break;
            case 'back_link':
                $this->setValue($row, $value);
                break;
            case 'touch':
                $this->setValue($row, $value);
                break;
            case 'mail_message':
                $this->setValue($row, $value);
                break;
            case 'contact_mail':
                $this->setValue($row, $value);
                break;
        }

    }

    public function setValue($row, $value)
    {
        $result = $this->setMysqli()->query("UPDATE lookAndFeel SET $row='$value'");
        if ($result) {
            $data = ['message'=>'success'];
            return json_encode($data);
        } else {
            $data = ['message' => 'fail'];
            return json_encode($data);
        }
    }
}