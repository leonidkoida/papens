<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 15.03.15
 * Time: 0:19
 */

require_once 'connections/connections.php';

/**
 * @property mixed return
 */
class ConnectController {

    public $mysqli;

    /**
     * @return mysqli
     * @throws Exception
     */
    public function setMysqli()
    {
        $mysqli = $this->mysqli = new mysqli(HOST,USER,PASS,DB_NAME);
        if ($mysqli->connect_error) {
            throw new Exception('no connection with database');
        } else {
            return $mysqli;
        }

    }

}