<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 09.05.15
 * Time: 16:31
 */

require_once 'ConnectController.php';

/**
 * Class mainController
 */
class mainController extends ConnectController
{

    public $data = [];

    public $tags = [];

    /**
     * @param $login
     * @param $pass
     * @return array
     * @throws Exception
     */
    public function checkUserCredentials($login, $pass)
    {
        $query = $this->setMysqli();
        $Credentials = $query->query("SELECT login, user_pass
                                      FROM Users
                                       WHERE login='$login' AND user_pass='$pass'");
        if ($Credentials->num_rows != 1) {
            $this->data[] = 'you have bad credentials';
        } else {
            while ($row = $Credentials->fetch_array(MYSQLI_NUM)) {
                $this->data[] = $row;
            }
            $this->data[] = 'you authorized as ' . $login;
        }
        $query->close();
        return $this->data;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getSiteName()
    {
        $query = $this->setMysqli();
        $result = $query->query("SELECT name FROM SystemText WHERE system_name='site_name'");
        if ($result->num_rows>0) {
            while ($row = $result->fetch_assoc()) {
                $this->data = $row;
            }
        } else {
            $this->data = ['name' => 'Sitename'];
        }
        return $this->data;
    }
    /**
     * @param $tags
     * @return array
     * @throws Exception
     */
    public function addTags($tags)
    {
        $tag = $tags;
        $query = $this->setMysqli();
        $result = $query->query("SELECT creative_field FROM Creative_Fields WHERE creative_field='$tag'");
        if ($result->num_rows != 0) {
            $this->data['status'] = 'error';
            $this->data['msg'] = 'this tag already exist';
        } else {
            $result = $query->query("INSERT INTO Creative_Fields (creative_field) VALUES ('$tag')");
            if ($result) {
                $this->data['status'] = 'success';
                $this->data['msg'] = $tag;
            }
        }
        $query->close();
        return $this->data;
    }

    /**
     * @return array
     * @throws Exception
     * @method GET
     */
    public function getTags()
    {
        $result = $this->setMysqli()->query("SELECT * FROM Creative_Fields");
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $this->tags[] = $row;
            }
        }
        $result->close();
        return $this->tags;
    }

    /**
     * @param array $tag
     * @return array data
     */
    public function updateTags(array $tag)
    {
        $id = trim(htmlspecialchars($tag['tagUpdateControl']));
        $name = trim(htmlspecialchars($tag['updatedTag']));
        $result = $this->setMysqli();

        if (!is_numeric($id)) {
            $this->data[] = 'Id must be a digit';
        } else {
            $newTag = $result->query("UPDATE Creative_Fields SET creative_field='$name' WHERE id='$id'");
            if ($newTag) {
                $this->data['success'] = $name;
            } else {
                $this->data['error'] = 'can\'t update tag';
            }
        }
        return $this->data;
    }

    /**
     * @param $tag
     * @return array
     * @throws Exception
     */
    public function deleteTags($tag)
    {
        $query = $this->setMysqli();

        $result = $query->query("DELETE FROM Creative_Fields WHERE id='$tag' LIMIT 1");
        if ($result) {
            $this->data['success'] = 'tag was successfully removed';
        } else {
            $this->data['error'] = 'tag can\'t be remove';
        }

        $query->close();
        return $this->data;
    }

    /**
     * @param $users
     * @return array
     * @throws Exception
     */
    public function addUser($users)
    {
        //todo need to encode this data
        $login = htmlspecialchars(trim($users['newLogin']));
        $mail = htmlspecialchars(trim($users['userMail']));
        $pass = md5(htmlspecialchars(trim($users['userPass'])));

        $query = $this->setMysqli();
        if (empty($login) || empty($mail) || empty($pass)) {
            $this->data[] = 'please fill all required fields';
        } else {
            $result = $query->query("SELECT login, user_pass FROM Users WHERE login='$login'");
            if ($result->num_rows > 0) {
                $this->data['warning'] = 'Login ' . $login . ' is already in use';
                return $this->data;
            } else {
                $result = $query->query("INSERT INTO Users(login,email, user_pass) VALUES('$login', '$mail', '$pass')");
                if ($result) {
                    $this->data['success'] = 'user added successfully';
                } else {
                    $this->data['error'] = 'something goes wrong try one more time or send mail to admin';
                }
            }
        }
        $query->close();
        return $this->data;
    }

    /**
     * @param array $userData
     * @return array
     * @throws Exception
     */
    public function updateUser(array $userData)
    {
        /*new user data*/
        $id = trim(htmlspecialchars($userData['userId']));
        $login = trim(htmlspecialchars($userData['updateLogin']));
        $updateEmail = trim(htmlspecialchars($userData['updateEmail']));
        $currentPass = trim(htmlspecialchars($userData['currentPass']));
        $check = trim(htmlspecialchars($userData['check']));

        if (!is_numeric($id)) {
            $this->data[] = 'user id must be integer';
        } else {
            $user = $this->setMysqli()->query("SELECT * FROM Users WHERE id='$id'");
            if ($user->num_rows<1) {
                $this->data[] = 'no such user in data base';
            } else {
                while ($row = $user->fetch_assoc()) {
//                    print_r($row['id']);
                    if ($login != $row['login']) {
                        $this->setMysqli()->query("UPDATE Users SET login='$login' WHERE id='$id'");
                    }
                    if ($updateEmail != $row['email']) {
                        $this->setMysqli()->query("UPDATE Users SET email='$updateEmail' WHERE id='$id'");
                    }
                    if (isset($currentPass) && ($currentPass == $check)) {
                        $currentPass = md5($currentPass);
                        if ($currentPass != $row['password']) {
                            $this->setMysqli()->query("UPDATE Users SET password='$currentPass' WHERE id='$id'");
                        }
                    }
                }
                $this->data[] = 'user updated successfully';
            }
        }
        return $this->data;
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function deleteUser($id)
    {
        $findUser = $this->setMysqli()->query("SELECT * FROM Users WHERE id='$id'");
        if ($findUser->num_rows > 0) {
            $deleteUser = $this->setMysqli()->query("DELETE FROM Users WHERE id='$id' LIMIT 1");
            if ($deleteUser) {
                $this->data['success'] = 'user #' . $id . ' deleted successfully';
            } else {
                $this->data['error'] = 'some error happened during deleting user # ' . $id;
            }
        }
        return $this->data;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getAllUsers()
    {
        $result = $this->setMysqli()->query("SELECT * FROM Users");
        $users = [];
        if ($result->num_rows>0) {
            while ($row = $result->fetch_assoc()) {
                $users[] = $row;
            }
        }
        return $users;
    }

    /**
     * @param array $post
     * @return array
     * @throws Exception
     */
    public function updateSystemInfo(Array $post)
    {
        $systemInfo = $post['SystemInfo'];
        $id = trim(htmlspecialchars($post['systemInfoId']))*1;
        $updateSystemInfo  = $this->setMysqli()->query("UPDATE SystemText SET name='$systemInfo' WHERE id='$id'");
        if (!$updateSystemInfo) {
            $this->data[] = 'Error on update field';
        } else {
            $this->data[] = 'info successfully updated';
        }
        return $this->data;
    }


    /**
     * @return array
     * @throws Exception
     */
    public function getAllSystemInfo()
    {
        $systemInfo = $this->setMysqli()->query("SELECT * FROM SystemText ORDER BY id ASC ");
        if ($systemInfo->num_rows>0) {
            while ($row = $systemInfo->fetch_assoc()) {
                $this->data[] = $row;
            }
        } else {
            $this->data[] = 'error';
        }
        return $this->data;
    }

    /**
     * @return array|bool|string
     * @throws Exception
     */
    public function getIcon()
    {
        $connect = $this->setMysqli();

        $result = $connect->query("SELECT icon_image_url
                                   FROM PersonalInfo LIMIT 1");
        $icon = '';
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $icon = $row;
            }
        } else {
            $icon = false;
        }
        $connect->close();
        return $icon;
    }

    public function getEmails()
    {
        $connect = $this->setMysqli();

        $result = $connect->query("SELECT *
                                   FROM user_emails");
        $emails = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $emails[] = $row;
            }
        }else{
            $emails = false;
        }

        $connect->close();
        return $emails;
    }
}
