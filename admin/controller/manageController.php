<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 26.03.15
 * Time: 1:29
 */
require_once "ConnectController.php";

/**
 * Class manageController
 */
class manageController extends ConnectController
{

    public $current_project = [];

    public $msg = [];

    public $old_image;

    public $project_name;

    /**
     * @return array
     * @throws Exception
     */
    public function getProjectsInTable()
    {
        $get_projects = $this->setMysqli();
        $result = $get_projects->query("SELECT *
                                        FROM t_Project
                                        ORDER BY priority ASC ");
        $projects_array = [];

        while ($row = $result->fetch_assoc()) {
            $row['creativeFields'] = [];
            $tags = $get_projects->query("
            SELECT c.creative_field, c.id
            FROM Creative_Fields c  JOIN index_creative i
            On i.creative_field_id = c.id
            WHERE i.project_id = '$row[id]'
            ");
            if ($tags->num_rows>0) {
                while ($tag = $tags->fetch_assoc()) {
                    array_push($row['creativeFields'], $tag);
                }
            } else {
                 $row['creativeFields'] = [0=>['creative_field' => 'no tags']];
            }
            $projects_array[] = $row;
        }
        $get_projects->close();
        return $projects_array;
    }

    /**
     * @param $project_id
     * @return array
     * @throws Exception
     */
    public function getProject($project_id)
    {
        $get_current_project = $this->setMysqli();
        $result = $get_current_project->query("SELECT * FROM t_Project WHERE id='$project_id'");
        while ($row = $result->fetch_assoc()) {
            $row['creativeFields'] = [];

            //TODO set this part of query into helper Query class
            $tags = $get_current_project->query("
            SELECT c.creative_field, c.id
            FROM Creative_Fields c  JOIN index_creative i
            On i.creative_field_id = c.id
            WHERE i.project_id = '$row[id]'
            ");
            if ($tags->num_rows>0) {
                while ($tag = $tags->fetch_assoc()) {
                    array_push($row['creativeFields'], $tag);
                }
            } else {
                $row['creativeFields'] = [0=>['creative_field' => 'no tags']];
            }
            $this->current_project['project'] = $row;
        }
        $get_current_project->close();
        $this->current_project['project']['pub_date'] = date('Y-m-d', strtotime($this->current_project['project']['pub_date']));
        return $this->current_project;
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function getProjectName($id)
    {
        $result = [];
        $getName = $this->setMysqli()->query("SELECT projectName FROM t_Project WHERE id='$id' LIMIT 1");
        if ($getName->num_rows>0) {
            while ($row = $getName->fetch_assoc()) {
                $result = $row;
            }
        }
        return $result;
    }

    /**
     * @param $files
     * @param $post
     * @return array
     * @throws Exception
     */
    public function updateProject($files, $post)
    {
     /*old settings*/
        $old_project = self::getProject($post['updateProject']);

        $old_project = $old_project['project'];
        $project_id = $old_project['id'];
        $old_name = $old_project['projectName'];
        $old_date = $old_project['pub_date'];
        $old_client = $old_project['client'];
     /*--------------------------------------------------*/

        /*new settings*/
        $new_name = trim(htmlspecialchars($post['projectName']));
        $new_system_name = preg_replace('/\s/', '_', $new_name);

        $new_date = $post['projectDate'];
        $time = strtotime($new_date);
        $new_date = date('Y-m-d', $time);
        print_r($new_date);
        $new_creative = json_decode($post['creative']);
        $new_description = trim($post['projectDescription']);
        $new_client = trim(htmlspecialchars($post['projectClient']));

        $img = $files['projectImg']['name'];
        $img_tmp_name = $files['projectImg']['tmp_name'];
        $img_errors = $files['projectImg']['error'];
     /*-----------------------------------------------------*/

        $upload_changes = $this->setMysqli();

        if ($new_system_name != $old_name) {
            if (file_exists(PATH_TO_PROJECT.$old_project['projectName'])) {
                rename(PATH_TO_PROJECT . $old_project['projectName'], PATH_TO_PROJECT . $new_system_name);
                $upload_changes->query("UPDATE t_Project SET projectName='$new_system_name' WHERE id='$project_id'");
                $this->msg[] = 'this directory exist';
                $this->msg[] = 'project name changed<br>';
            } else {
                echo 'no found directory '. $old_name. ' in project.';
            }
        }
        if (!empty($img) && $img_errors == 0) {
//            print_r($files);
            $find_project_img = $upload_changes->query("SELECT projectImage, projectName
                                                        FROM t_Project
                                                        WHERE id='$project_id' LIMIT 1");
            if ($find_project_img->num_rows > 0) {
                while ($row = $find_project_img->fetch_assoc()) {
                    $this->old_image = $row['projectImage'];
                    $this->project_name = $row['projectName'];
                }
                if ($this->old_image != $img) {
                    $this->msg[] = 'not same image';
                } else {
                    $this->msg[] = 'images are identical';
                    return $this->msg;
                }
                if (file_exists(PATH_TO_PROJECT.$this->project_name.'/project_header_img/'.$this->old_image)) {
                    //TODO Here is unlink function
                    unlink(PATH_TO_PROJECT.$this->project_name.'/project_header_img/'.$this->old_image);
                }
                $upload_changes->query("UPDATE t_Project SET projectImage='$img' WHERE id='$project_id'");

                $upload_url = PATH_TO_PROJECT.$this->project_name.'/project_header_img/'. $img;
                move_uploaded_file($img_tmp_name, "$upload_url");
            }
        }

        if (!empty($new_creative)) {
            foreach ($new_creative as $creative) {
                $upload_changes->query("INSERT INTO index_creative (creative_field_id, project_id)
                                        VALUES ($creative, $project_id)");
            }
        }
        if (!empty($new_client) && $new_client != $old_client) {
            $upload_changes->query("UPDATE t_Project SET client='$new_client' WHERE id='$project_id'");
        }
        if ($old_date != $new_date) {
            $upload_changes->query("UPDATE t_Project SET pub_date='$new_date' WHERE id='$project_id'");
        }
        if (!empty($new_description)) {
             $upload_changes->query("UPDATE t_Project
                                     SET projectDescription='$new_description'
                                     WHERE id='$project_id'");
        }
        if (empty($new_description)) {
            $upload_changes->query("UPDATE t_Project
                                     SET projectDescription=null
                                     WHERE id='$project_id'");
        }
        $upload_changes->close();
        return $this->msg;
    }

    /**
     * @param $post
     * @internal param $id
     * @return array
     */
    public function deleteCreativeField($post)
    {
        $pId = $post['deleteTagFromProject']['projectId'];
        $cId = $post['deleteTagFromProject']['tagId'];
        // TODO create validation for existing project and creative field with this id

        $result = $this->setMysqli()->query("DELETE
                                             FROM index_creative
                                             WHERE project_id={$pId} AND creative_field_id={$cId} LIMIT 1");
        if ($result) {
            $this->msg['success'] = 'deleted successfully';
            $this->msg['id'] = $cId;
        } else {
            $this->msg['error'] = 'can\'t delete ';
        }
        return $this->msg;
    }

    /**
     * @param $status
     * @param $project_id
     * @return string
     * @throws Exception
     */
    public function changeStatus($status, $project_id)
    {
        $result = $this->setMysqli()->query("UPDATE t_Project SET status='$status' WHERE id='$project_id'");
        if (!$result) {
             $this->msg[] = 'something goes wrong';
            //TODO create \Exception
        } else {
             $this->msg[] = 'status changed';
        }
        return $this->msg;
    }

    /**
     * @param $id
     * @param $priority
     * @return string
     * @throws Exception
     */
    public function changePriority($id, $priority)
    {
        $result = $this->setMysqli()->query("UPDATE t_Project SET priority='$priority' WHERE id='$id'");
        if (!$result) {
             $this->msg[] = 'can\'t update priority cell';
        } else {
             $this->msg[] = 'priority changed';
        }
        return $this->msg;
    }

    /**
     * @param $id
     * @return string
     * @throws Exception
     */
    public function deleteProject($id)
    {
        $projectToDelete = $this->setMysqli()->query("SELECT projectName FROM t_Project WHERE id='$id' LIMIT 1");
        if ($projectToDelete->num_rows > 0) {
            $projectToDelete = $projectToDelete->fetch_array(MYSQL_NUM);
            $pathToProject = PATH_TO_PROJECT.$projectToDelete[0];
            $name = preg_replace('/_/', ' ', $projectToDelete[0]);
            if (file_exists($pathToProject)) {
                self::deleteDirectory($pathToProject);
                $this->setMysqli()->query("DELETE FROM t_Project WHERE id='$id' LIMIT 1");
                $this->msg[] = 'project ' . $name . " was successfully deleted";
            } else {
                $this->msg[] = 'no such file or directory in the project';
            }
        } else {
            $this->msg[] = 'no such project in database';
        }
        return $this->msg;
    }

    /**
     * @param $directory
     */
    private function deleteDirectory($directory)
    {
        if (is_dir($directory)) {
            $objects = scandir($directory);
            foreach ($objects as $obj) {
                if ($obj !="." && $obj !="..") {
                    if (filetype($directory . "/".$obj) == "dir") {
                        self::deleteDirectory($directory . "/" . $obj);
                    } else {
                        unlink($directory . '/' .$obj);
                    }
                }
            }
            reset($objects);
            rmdir($directory);
        }
    }

    /**
     * @param $projectId
     * @throws Exception
     * @return bool|mysqli_result
     */
    public function getProjectItems($projectId)
    {
        //TODO написать исключение если что то  пошло не так
        $query = $this->setMysqli();
        $items = $query->query("SELECT pI.id, pI.image_url, pI.project_id, p.projectName, pI.description, pI.priority, pI.status
                   FROM ProjectItems pI INNER JOIN t_Project p
                        ON p.id = pI.project_id
                   WHERE pI.project_id='$projectId'
                   ORDER BY pI.priority DESC;");

        $links = $query->query("SELECT V.id, V.link, V.description, V.priority, V.status, P.projectName
                                FROM Video_links V INNER JOIN t_Project P
                                ON V.project_id = P.id
                                 WHERE V.project_id = '$projectId'");

        if ($items->num_rows>0) {
            while ($row = $items->fetch_assoc()) {
                $this->msg[] = $row;
            }
        }
        if ($links->num_rows > 0) {
            while ($row = $links->fetch_assoc()) {
                $this->msg[] = $row;
            }
        }
        $query->close();

        return $this->msg;
    }

    /**
     * @param $id
     * @param $dataSrc
     * @param $priority
     * @return array
     * @internal param $post
     */
    public function updateProjectItemPriority($id, $dataSrc, $priority)
    {
        if ($id !=null && $priority != null) {
            $findItem = $this->setMysqli();
            if ($dataSrc === 'video-container') {
                $findVideo = $findItem
                ->query("UPDATE Video_links SET Video_links.priority='$priority' WHERE Video_links.id = '$id'");
                if (!$findVideo) {
                    $this->msg[] = 'some trouble during update priority of the item';
                } else {
                    $this->msg[] = 'priority successfully updated';
                }
            } elseif ($dataSrc === 'figure') {
                $findImage = $findItem
                    ->query("UPDATE ProjectItems SET ProjectItems.priority='$priority' WHERE ProjectItems.id = '$id'");
                if (!$findImage) {
                    $this->msg[] = 'some trouble during update priority of the item';
                } else {
                    $this->msg[] = 'priority successfully updated';
                }
            }
            $findItem->close();
        }
        return $this->msg;
    }

    /**
     * @param $id
     * @param $dataSrc
     * @param $status
     * @return array
     * @throws Exception
     */
    public function updateProjectItemStatus($id, $dataSrc, $status)
    {
        if ($id != null && $status != null) {
            $findItem = $this->setMysqli();
            if ($dataSrc === 'video-container') {
                $findPicture = $findItem
                    ->query("UPDATE Video_links SET Video_links.status='$status' WHERE Video_links.id = '$id'");
                if (!$findPicture) {
                    $this->msg[] = 'some trouble during update status of the item';
                } else {
                    $this->msg[] = 'status successfully updated';
                }
            } elseif ($dataSrc === 'figure') {
                $findVideo = $findItem
                    ->query("UPDATE ProjectItems SET ProjectItems.status='$status' WHERE ProjectItems.id = '$id'");
                if (!$findVideo) {
                    $this->msg[] = 'some trouble during update status of the item';
                } else {
                    $this->msg[] = 'status successfully updated';
                }
            }
            $findItem->close();
        } else {
            $this->msg[] = 'you have some empty values';
        }

        return $this->msg;
    }

    /**
     * @param $id
     * @param $src
     * @return array
     * @throws Exception
     */
    public function getItemsDescription($id, $src)
    {
        if (!empty($id) || is_numeric($id)) {
            $getDescription = $this->setMysqli();
            if ($src === 'figure') {
                $itemDescription = $getDescription->query("
                SELECT description FROM ProjectItems WHERE id='$id' LIMIT 1");
                if ($itemDescription->num_rows > 0) {
                    while ($row = $itemDescription->fetch_assoc()) {
                        $this->msg[] = $row;
                    }
                }
            } elseif ($src === 'video-container') {
                $itemDescription = $getDescription->query("
                SELECT description FROM Video_links WHERE id='$id'");
                if ($itemDescription->num_rows > 0) {
                    while ($row = $itemDescription->fetch_assoc()) {
                        $this->msg[] = $row;
                    }
                }
            }
            $getDescription->close();
        }
        return $this->msg;
    }

    /**
     * @param $post
     * @throws Exception
     * @return array
     */
    public function updateItemDescription($post)
    {
        $id = trim(htmlspecialchars($post['itemId']));
        $description = $post['itemDescription'];
        $dataSrc = trim(htmlspecialchars($post['src']));
        if (!is_numeric($id)) {
            http_response_code(404);
            echo json_encode(['data' => 'values must be a numbers only']);
        } else {
            if ($id != null) {
                $findItem = $this->setMysqli();
                if ($dataSrc === 'figure') {
                    $updateImage = $findItem
                        ->query("UPDATE ProjectItems P SET P.description='$description' WHERE P.id = '$id'");
                    if (!$updateImage) {
                        $this->msg[] = 'something goes wrong during update description';
                    } else {
                        $this->msg[] = 'description updated successfully';
                    }
                } elseif ($dataSrc === 'video-container') {
                    $updateLink = $findItem
                        ->query("UPDATE Video_links V SET V.description='$description' WHERE V.id='$id' ");
                    if (!$updateLink) {
                        $this->msg[] = 'something goes wrong during update description';
                    } else {
                        $this->msg[] = 'description updated successfully';
                    }
                }
                $findItem->close();
            }
        }
        return $this->msg;
    }
    /**
     * @param $id
     * @param $dataSrc
     * @return array
     * @throws Exception
     */
    public function deleteItem($id, $dataSrc)
    {
        if (!empty($id)) {
            if ($dataSrc === 'video-container') {
                $deleteLink = $this->setMysqli();
                $item = $deleteLink->query("DELETE FROM Video_links WHERE id='$id' LIMIT 1");
                if (!$item) {
                    $this->msg[] = 'some trouble during deleting item';
                } else {
                    $this->msg[] = 'item successfully deleted';
                }
                $deleteLink->close();
            } else {
                //TODO delete image from file system)))
                $ItemToDelete = $this->setMysqli()->query("SELECT p.projectName, i.image_url
                                                   FROM ProjectItems i INNER JOIN t_Project p
                                                   ON p.id = i.project_id
                                                   WHERE i.id='$id' LIMIT 1");
                $ItemToDelete = $ItemToDelete->fetch_array(MYSQL_NUM);
                $pathToItem = PATH_TO_PROJECT.$ItemToDelete[0] . '/items/' . $ItemToDelete[1];
                unlink($pathToItem);
                $deleteItem = $this->setMysqli()->query("DELETE FROM ProjectItems WHERE ProjectItems.id = '$id' LIMIT 1");
                if (!$deleteItem) {
                    $this->msg[] = 'some trouble during deleting item';
                } else {
                    $this->msg[] = 'item successfully deleted';
                }
            }
        } else {
            $this->msg[] = 'you have not ID for deleting item';
        }

        return $this->msg;
    }
}
