<div class="modal fade" id="UpdateLinkModal" tabindex="-1" role="dialog" aria-labelledby="UpdateLinkModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="UpdateLinkLabel"><span>Link: </span><span id="name"></span></h4>
            </div>`
            <div class="modal-body">
                <form method="post" id="UpdateLinkForm">
                    <div class="form-group">
                        <label for="linkName" class="control-label">Link name:</label>
                        <input type="text" name="linkName" required="required" id="linkName"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="linkAddress">link address:</label>
                        <input id="linkAddress" type="text" required="required" name="linkAddress"/>
                    </div>
                    <input type="hidden" name="linkUpdateControl" value="link"/>
                    <input type="hidden" name="linkId" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="m-btn m-btn-group small red" data-dismiss="modal">close</button>
                <button type="button" id="UpdateLink" class="m-btn m-btn-group small blue">add</button>
            </div>
        </div>
    </div>
</div>