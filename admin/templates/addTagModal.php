<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">New tag</h4>
            </div>`
            <div class="modal-body">
                <form method="post" id="addTagForm">
                    <div class="form-group">
                        <label for="new-tag" class="control-label">Tag:</label>
                        <input type="text" name="newTag" required="required" id="new-tag"/>
                        <input type="hidden" name="tagControl" value="tag"/>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="m-btn m-btn-group small red" data-dismiss="modal">close</button>
                <button type="button" id="addTags" class="m-btn m-btn-group small blue">add</button>
            </div>
        </div>
    </div>
</div>