<form action="" method="post" id="uploadProject" enctype="multipart/form-data">
<?php if (isset($project)) {
    $name = preg_replace('/_/', ' ', $project['project']['projectName']);
    ?>
    <div class="row">
        <div class="col-md-3 col-md-offset-2">
            <h3 class="text-muted"><?php echo $name; ?></h3>
        </div>
    </div>
    <?php
} else {
}
?>
        <!--project name-->
        <div class="row">
            <div class="col-md-4"><label class="field" for="prName">Project name</label> </div>
            <div class="col-md-8"><input type="text" name="prName" value="" id="prName"/></div>
        </div>
        <!--Project image-->
        <div class="row">
            <div class="col-md-4"><label class="field" for="prFile">Upload project image</label> </div>
            <div class="col-md-8">
                <input type="hidden" name="MAX_FILE_SIZE" value="4194304" />
                <label class="fileUploaderInput alert-success" style="width: 250px;">
                    <span>
                        <i class="fa fa-file-image-o" style="font-size: large;"></i>
                        <span class="img_path">Select image</span>
                    </span>
                    <input type="file" name="uploadProjectImage" id="prFile"  style="display: none;"/>
                    <span class="imageIndicator"></span>
                </label>
            </div>
        </div>
        <!--creative fields-->
        <div class="row">
            <div class="col-md-4"><label class="field" for="prField">Creative fields</label> </div>
            <div class="col-md-8">
                <select multiple name="c_fields[]" id="prField">
                    <?php
                    foreach ($creativeFields as $field) {
                        echo "<option data-creative-id='" . $field['id'] . "' value='". $field['creative_field'] ."_" . $field['id'] . "'>" . $field['creative_field'] . "</option>";
                    }
                    ?>
               </select>
            </div>
        </div>
        <!--project post date-->
        <div class="row">
            <div class="col-md-4"><label class="field" for="prDate">Date</label> </div>
            <div class="col-md-8"><input type="text" name="prDate" id="prDate" value=""/></div>
        </div>
        <!--poject Client-->
        <div class="row">
            <div class="col-md-4"><label class="field" for="prClient">Client</label> </div>
            <div class="col-md-8"><input type="text" name="prClient" id="prClient" value=""/></div>
        </div>
    <!--project description-->
        <div class="row">
            <div class="col-md-4">
                <label class="field" for="prDescription">Description</label>
            </div>
            <div class="col-md-8">
                <button type="button" class="m-btn m-btn-group blue" id="prDescription">add description</button>
            </div>
        </div>
    <div class="row" id="description-container" style="display: none;">
        <div class="col-md-12">
    <textarea class="text-editor" name="projectDescription" id="prDescr"></textarea>
       </div>
    </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-8">
                <div class="btn-group">
                    <button type="submit" name="saveProject"  value="projectSave" class="m-btn m-btn-group blue">save</button>
                    <input name="saveProject" value="projectSave" type="hidden"/>
                    <button type="reset" class="m-btn m-btn-group red">reset</button>
                </div>
            </div>
        </div>
    </form>
