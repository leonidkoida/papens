<?php
require_once 'controller/mainController.php';
$icon = new mainController();
$currentIcon  = $icon->getIcon();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>look and feel</title>
    <?php
    $urlToIcon = '';
    if (!empty($currentIcon['icon_image_url'])) {
        $urlToIcon = '/Assets/img/system/logo/icons/' . $currentIcon['icon_image_url'];
    } else {
        $urlToIcon = '/Assets/img/system/logo/icons/default/favicon-96x96.png';
    }
    ?>
    <link rel="icon" href="<?php echo $urlToIcon; ?>" type="image/x-icon"/>
    <link rel="stylesheet" href="/Assets/metro-style-css/bootstrap.css"/>
    <link rel="stylesheet" href="/Assets/metro-style-css/css/m-styles.min.css"/>
    <link rel="stylesheet" href="/Assets/metro-style-css/bootstrap-datepicker3.css"/>
    <link rel="stylesheet" href="/Assets/fa/css/font-awesome.css"/>
    <link rel="stylesheet" href="/Assets/metro-style-css/notify-metro.css"/>
    <link rel="stylesheet" href="/Assets/metro-style-css/admin.css"/>
</head>
<body>
<div class="container admin">

