<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header" style="padding: 8px">
                <h4>You really want to delete  this item?</h4>
            </div>
            <div class="modal-body" >
                <div class="btn-group text-center">
                    <button type="button" class="m-btn m-ctrl-medium green" data-dismiss="modal" data-delete-item=0>no</button>
                    <button type="button" id="deleteItem" class="m-btn m-ctrl-medium red" data-delete-item=1>yes</button>
                </div>
            </div>
        </div>
    </div>
</div>