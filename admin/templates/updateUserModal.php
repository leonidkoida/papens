<div class="modal fade" id="updateUserModal" tabindex="-1" role="dialog" aria-labelledby="updateUserModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="updateUserLabel"><span>User: </span><span id="name"></span></h4>
            </div>`
            <div class="modal-body">
                <form method="post" id="updateUserForm">
                    <div class="form-group">
                        <label for="updateLogin" class="control-label">user login:</label>
                        <input type="text" name="updateLogin" required="required" id="updateLogin"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="updateEmail">user email:</label>
                        <input id="updateEmail" type="text" required="required" name="updateEmail"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="currentPass">user password:</label>
                        <input id="currentPass" type="password" name="currentPass"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="checkPass">repeat password:</label>
                        <input type="password" name="check" id="checkPass"/>
                    </div>
                    <input type="hidden" name="usersUpdateControl" value="user"/>
                    <input type="hidden" name="userId" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="m-btn m-btn-group small red" data-dismiss="modal">close</button>
                <button type="button" id="updateUser" class="m-btn m-btn-group small blue">add</button>
            </div>
        </div>
    </div>
</div>