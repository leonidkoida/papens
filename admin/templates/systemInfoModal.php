<div class="modal fade" id="systemInfoModal" tabindex="-1" role="dialog" aria-labelledby="systemInfoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="systemInfoLabel"><span>systemInfo: </span><span id="name"></span></h4>
            </div>
            <div class="modal-body">
                <form method="post" id="addSystemInfo">
                    <div class="form-group text-center">
                        <label>
                            <textarea class="text-editor" name="SystemInfo" id="systemInfo" cols="80" rows="10"></textarea>
                        </label>
                    </div>
                    <input type="hidden" name="systemInfoUpdateControl" value="systemInfo"/>
                    <input type="hidden" name="systemInfoId" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="m-btn m-btn-group small red" data-dismiss="modal">close</button>
                <button type="button" id="addInfo" class="m-btn m-btn-group small blue">add</button>
            </div>
        </div>
    </div>
</div>