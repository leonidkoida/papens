<div class="modal fade" id="updateTagModal" tabindex="-1" role="dialog" aria-labelledby="updateTagModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="updateTagModalLabel">New tag</h4>
            </div>`
            <div class="modal-body">
                <form method="post" id="UpdateTagForm">
                    <div class="form-group">
                        <label for="new-tag" class="control-label">Tag:</label>
                        <input type="text" name="updatedTag" required="required" id="new-tag"/>
                        <input type="hidden" name="tagUpdateControl" value=""/>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="updateTag" class="m-btn m-btn-group small blue">add</button>
                <button type="button" class="m-btn m-btn-group small red" data-dismiss="modal">close</button>
            </div>
        </div>
    </div>
</div>