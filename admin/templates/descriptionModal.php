<div class="modal fade" id="descriptionModal" tabindex="-1" role="dialog" aria-labelledby="descriptionLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="descriptionLabel">
                    <span>Description: </span><span id="name">
                    </span>
                </h4>
            </div>
            <div class="modal-body">
                <form method="post" id="addProjectDescription">
                    <div class="form-group text-center">
                        <label>
                            <textarea class="text-editor" form="uploadProject" name="projectDescription" id="prDescr" cols="90" rows="10"></textarea>
                        </label>
                    </div>
                    <input type="hidden" name="usersUpdateControl" value="user"/>
                    <input type="hidden" name="userId" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="addDescription" class="m-btn m-btn-group small blue">add</button>
                <button type="button" class="m-btn m-btn-group small red" data-dismiss="modal">close</button>
            </div>
        </div>
    </div>
</div>