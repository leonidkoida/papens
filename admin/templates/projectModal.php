<div class="modal fade" id="updateProjectModal" tabindex="-1" role="dialog" aria-labelledby="myUpdateProjectLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <form action="" enctype="multipart/form-data" id="updateProjectForm" method="post" name="updateProjectForm">
                    <div class="row">
                        <div class="col-md-6">
                            <!--all inputs-->
                            <input type="text" name="projectName" id="projectName"/>
                            <p id="projectCreative" style="width: 250px;"></p>
                            <select id="selectProject" name="creativeFields[]" multiple style="width: 250px;">
                            <?php
                            $fields = $select->getCreativeFields();
                            foreach ($fields as $value) {
                                echo "<option data-creative-id='{$value['id']}' value='{$value['creative_field']}' id='{$value['id']}' >{$value['creative_field']}</option>";
                            }
                            ?>
                            </select>
                            <!--TODO this is the template of styling input type file in this project -->
                            <label for="projectImg" class="alert-success fileUploaderInput">
                                <span>
                                    <i class="fa fa-file-image-o" style="font-size: large;"></i>
                                    <span class="img_path">Select image</span>
                                </span>
                                <input type="file" name="projectImg" id="projectImg" style="display:none;"/>
                            </label>
                            <label for="prDate">
                                <input type="text" name="projectClient" id="projectClient"/>
                            </label><input type="text" name="projectDate" id="prDate"/>

                            <input name="updateProject" id="projectId" type="hidden"/>
                        </div>
                        <div class="col-md-6 text-center">
                            <!--current project image-->
                            <img id="currentImage" src="" class="img-responsive" style="padding-left: 10px; margin: auto;" alt=""/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="margin-top: 10px;">
                            <!--<div>
                                <button class="m-btn m-btn-group small red left" name="clearText">
                                    of
                                </button>
                            </div>-->
                            <textarea class="text-editor" name="projectDescription" id="projectDescription" placeholder="description" cols="98" rows="10"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" id="updateProject" class="m-btn m-btn-group green">Update</button>
                <button type="reset" class="m-btn m-btn-group blue">reset</button>
                <button type="button" class="m-btn m-btn-group red" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>