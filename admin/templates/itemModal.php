<div class="modal fade" id="updateItemModal" tabindex="-1" role="dialog" aria-labelledby="myUpdateItemLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content" style="width: auto;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalItemLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <form action="" enctype="multipart/form-data" id="updateProjectItems" method="post" name="updateProjectItems">
                    <div class="container">
                        <div class="row projectItems">
                            <?php


                            ?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" id="updateItems" class="m-btn m-btn-group green">Update</button>
                <button type="reset" class="m-btn m-btn-group blue">reset</button>
                <button type="button" class="m-btn m-btn-group red" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>