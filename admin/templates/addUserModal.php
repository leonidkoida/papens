<div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">New User</h4>
            </div>`
            <div class="modal-body">
                <form method="post" id="addUserForm">
                    <div class="form-group">
                        <label for="newLogin" class="control-label">user login:</label>
                        <input type="text" name="newLogin" required="required" id="newLogin"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="userMail">user email:</label>
                        <input id="userMail" type="text" required="required" name="userMail"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="userPass">user password:</label>
                        <input id="userPass" type="password" required="required" name="userPass"/>
                    </div>

                    <input type="hidden" name="usersControl" value="users"/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="m-btn m-btn-group small red" data-dismiss="modal">close</button>
                <button type="button" id="addUsers" class="m-btn m-btn-group small blue">add</button>
            </div>
        </div>
    </div>
</div>