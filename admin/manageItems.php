<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 14.04.15
 * Time: 22:23
 */
session_start();
require_once 'templates/header.php';
require_once 'controller/manageController.php';
if ($_GET['manage']) {
    $name = htmlspecialchars(trim($_GET['manage']));
    $update_items = new manageController();
    $items = $update_items->getProjectItems($name);
/*    echo "<pre>";
    foreach ($items as $row) {
        print_r($row['id']);
    }*/
//    print_r($items);
//    die;
    $projectName = $update_items->getProjectName($name);
    $projectName = preg_replace('/_/', ' ', $projectName['projectName']);
    ?>
    <div class="row main-wrapper">
    <div class="col-md-9 col-md-offset-1 content">
    <p class="small text-muted"><?php echo $_SESSION['greeting']; ?></p>
    <h2 class="left" style="color: deepskyblue;">Project: <?php echo $projectName; ?></h2>
    <div style="margin-top: 30px;">
                <form action="" method="post">
                <?php

                $counter = 1;

                echo "<div class='row'>";
                foreach ($items as $row) {
                    if ($row['description'] == '') {
                        $row['description'] = 'no description';
                    }
                    if ($row['status'] == 0) {
                        $statusButton =
                            "<a href='#' class='status-button red m-btn m-btn-group' data-status='{$row['id']}'>
                            <i class='fa fa-times' style='color: #ffffff'></i></a>";
                    } elseif ($row['status'] == 1) {
                        $statusButton =
                            "<a href='#' class='status-button green m-btn m-btn-group' data-status='{$row['id']}'>
                            <i class='fa fa-check' style='color: #ffffff'></i></a>";
                    }
                    $image_url ='/'.PATH_TO_UPLOAD.'/project_'. $row['projectName'] . '/items/' .$row['image_url'];
                    echo "<div class='col-md-4' style='margin-bottom: 50px; padding-top: 0;'>
                      <div class='row'>
                        <div class='manage-buttons' style='text-align: right;'>
                          <!-- status button -->
                          {$statusButton}
                          <a href='#' class='description-button blue m-btn m-btn-group' data-toggle='modal' data-target='#descriptionModal' data-description='{$row['id']}'>
                          <i class='fa fa-pencil-square-o' style='color: #ffffff'></i>
                          </a>
                          <a href='#' class='delete-button red m-btn m-btn-group' data-delete='{$row['id']}'>
                            <i class='fa fa-trash' style='color: #ffffff'></i></a>
                          <input type='text' class='priority'  style='width:35px;margin-left:5px; text-align: center;' data-priority='{$row['id']}' value='{$row['priority']}'>
                        </div>
                      </div>";
                    if ($row['image_url']) {
                        echo"<div class='figure' style = 'margin-top: 20px;'>
                        <p style = 'text-align:center;' >
                         <img class='project-image' src = '" . $image_url . "' />
                         </p >
                     </div>";
                    } elseif ($row['link']) {
                        echo "
                        <div class='video-container' style='margin-top: 20px;'>
                        <iframe width='262' height='177' src = '" . $row['link'] . "' frameborder='0' allowfullscreen></iframe>
                        </div>
                        ";
                    }
                    echo" </div>";
                    if ($counter%3==0) {
                        echo "</div><div class='row' >";
                    }
                    $counter++;
                }
}
echo "</div>";
                ?>
                </form>
            </div>
        </div>
        <div class="col-md-2 navigation">
            <div class="nav-module">
                <?php
                include_once "templates/sidebar.php";
                ?>
            </div>
        </div>
    </div>
    <?php
         include_once "templates/descriptionModal.php";
         include_once "templates/footer.php";
    ?>