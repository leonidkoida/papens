<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 12.03.15
 * Time: 11:28
 */
session_start();
if (empty($_SESSION['login']) && empty($_SESSION['pass']) && $_SESSION['error_msg'] != '') {
    $_SESSION['error_msg'] = 'you have bad credentials';
    header("Location: /admin/login.php", true, 301);
} else {
}
include_once "templates/header.php";
include_once "controller/uploadController.php";
require_once 'controller/manageController.php';

$select = new uploadController();
$manage = new manageController();
?>
<div class="row main-wrapper">
    <div class="col-md-9 col-md-offset-1 content">
        <!--<div class="container">-->
        <p class="small text-muted"><?php echo $_SESSION['greeting']; ?></p>
            <h1 class="left" style="color: deepskyblue;">Content</h1>
            <form action="" id="projectForm" method="post">
            <table  class="table"   id="projects_table">
                <tr>
                    <th style="text-align: center;"><i class="fa fa-book"></i> </th>
                    <th style="text-align: center"><i class="fa fa-bookmark"></i> </th>
                    <th style="text-align: center" id='image'><i class="fa fa-image"></i> </th>
                    <th style="text-align: center; padding: 0;"  id='status'><i class="fa fa-check"></i> </th>
                    <th style="text-align: center;" id='manage'><i class="fa fa-asterisk"></i> </th>
                    <th style="text-align: center; width: 40px;"><i class="fa fa-reorder"></i> </th>
                </tr>
                <?php
                $projects_table = $manage->getProjectsInTable();
                foreach ($projects_table as $value) {
                    $systemName = $value['projectName'];
                    $name =preg_replace('/_/', ' ', $value['projectName']);

                    if ($value['status']== 0) {
                        $value['status'] =
                            "<a class='m-btn m-btn-group red status-in-progress status' data-id='"
                                                  . $value['id'] . "' href='#' title='in progress'>
                                            <i class='fa fa-times' style='color: #ffffff;'></i>
                                            </a>";
                    } else {
                        $value['status'] =
                            "<a class='m-btn m-btn-group green status-active status' data-id='"
                                                    . $value['id'] . "' href='#' title='active'>
                                            <i class='fa fa-check' style='color: #ffffff;'></i>
                                            </a>";
                    }
                    echo"<tr>
                        <td style='text-align:left;'><strong>" . $name . "</strong></td>
                        <td style='text-align:left;'>";
                    foreach ($value['creativeFields'] as $tag) {
                            echo  "<span data-creative-id =" . $tag['id'] .  ">/" . $tag['creative_field'] . "</span> ";
                    }
                        echo "</td>
                        <td><img style='width:50px' src='/".PATH_TO_UPLOAD."/project_".$systemName."/project_header_img/" . $value['projectImage'] . "'/></td>
                        <td>
                        <div class='btn-group'>
                        " . $value['status'] . "
                        </div>
                        </td>
                        <td>
                        <div class='btn-group'>
                            <a class='m-btn m-btn-group blue' href='manageItems.php?manage=" . $value['id'] . "' title='show Items'>
                            <i class='fa fa-file-image-o' style='color: #ffffff;'></i>
                            </a>
                            <a class='m-btn m-btn-group green update' data-update='" . $value['id'] . "'  href='#' data-whatever='{$value["projectName"]}' title='update project' data-toggle='modal' data-target='#updateProjectModal'  style='background-color: green;'>
                            <i class='fa fa-pencil-square' style='color: #ffffff;'></i>
                            </a>
                            <a class='m-btn m-btn-group red delete' href='#' data-delete='" . $value['id'] . "' title='delete project' style='background-color: red;'>
                            <i class='fa fa-trash-o' style='color: #ffffff;'></i>
                            </a>
                            </div>
                            </td>
                        <td><input name='priority[]' class='priority' style='width:auto; max-width:30px; text-align:center;' type='text' data-project-id='" . $value['id'] . "' value='" . $value['priority'] . "' /></td>
                    </tr>";
                }
                ?>
            </table>
                </form>
        </div>
    <div class="col-md-2 navigation">
        <div class="nav-module">
            <?php
            include_once "templates/sidebar.php";
            ?>
        </div>
    </div>
</div>
<?php
include_once "templates/projectModal.php";
include_once "templates/itemModal.php";
include_once "templates/footer.php";