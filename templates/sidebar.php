<?php
/**
 * Created by PhpStorm.
 * User: leonid
 * Date: 11.03.15
 * Time: 23:38
 */
$menu = new defaultController();
$menu_items = $menu->test();
$linkMenu = $menu->getLinkMenu();
echo '<ul class="list-unstyled">';
if (!empty($_GET)) {
    echo "<li class='text-right'><a class='side-link' href='/'>all</a></li>";
}
foreach ($menu_items as $item) {
    $name = preg_replace('/_/', ' ', $item['projectName']);
    echo "<li class='text-right'>
             <a  class='side-link' href='project.php?project=".$item['projectName']."' >".$name."</a>
          </li>";
}
echo "</ul>";
?>
<ul class="list-unstyled second-menu">
    <?php
    foreach ($linkMenu as $linkItem) {
        if ($linkItem['link'] == '') {
            continue;
        }
        // TODO change href attribute? because if they will set url like this: http://anyname.com it will break all
        echo "<li class='text-right'>
                 <a class='link' target='_blank' href='" . $linkItem['link'] . "'>" . $linkItem['name'] . "</a>
             </li>";
    }
?>
<li class='text-right'>
    <a class='side-link' href="about.php">About</a>
</li><!--TODO если мы на станице то убрать из меню-->
<!--<li class="text-right" style="color: rgba(197, 198, 196, 0.94)">
     <a class="link" style="color: rgba(197, 198, 196, 0.94)" href="/admin/login.php">Inside</a>
</li>-->
</ul>