<?php
include_once'public_controllers/footerController.php';
$footer = new footerController();
$text = $footer->getData();
$copyright = preg_replace("/(<\/)/", "$2 ". date('Y') . "</", $text[7]['name']);
?>
        <div class="row footer">
            <div class="col-md-6 col-md-offset-2 footer-info" style="padding-left: 15px;">
                    <!--this is email in footer-->
                    <?php  echo $text[4]['name']; ?>
                <!--this is footer disclaimer-->
                    <?php echo $text[0]['name']; ?>
            </div>
            <div class="col-md-3 footer-info" style="padding-right: 31px;">
                <p class="text-right">
                    <a class="link" href="#top" style="color: rgba(113, 114, 112, 0.85)">Up</a>
                </p>
                    <?php  echo $copyright; ?>
            </div>
        </div>
</div>
<script src="/Assets/js/jquery-1.11.2.js"></script>
<script src="/Assets/js/bootstrap.js"></script>
<script src="/Assets/js/notify.js"></script>
<script src="/Assets/js/notify-metro.js"></script>
<script src="/Assets/js/public_js/public.js"></script>
</body>
</html>