<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php
    include_once 'public_controllers/defaultController.php';
    include_once'public_controllers/footerController.php';

    $links = new defaultController();
    $meta = $links->getMetaDescription();
    ?>
    <meta name="description" content="<?= preg_replace("/<\/?\w>/", "", $meta['name']); ?>" />
    <!-- You can use Open Graph tags to customize link previews.
   Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
 <!--   <meta property="og:url"           content="http://www.your-domain.com/your-page.html" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Your Website Title" />
    <meta property="og:description"   content="Your description" />
    <meta property="og:image"         content="http://www.your-domain.com/path/image.jpg" />-->
    <title>
        <?php
        if (isset($_GET) && !empty($_GET)) {
            foreach ($_GET as $name => $page) {
                $page = preg_replace('/_/', ' ', $page);
                echo $page . ' - StudioPaPe';
            }
        } elseif (empty($_GET)) {
            if ($_SERVER['REQUEST_URI'] ==='/index.php' || $_SERVER['REQUEST_URI'] === '/') {
                echo 'StudioPaPe';
            } elseif ($_SERVER['REQUEST_URI'] === '/about.php') {
                echo 'About - StudioPaPe';
            }
        }
        ?>
    </title>
    <?php
    $icon = $links->getIcon();
    $urlToIcon = '';
    if (!empty($icon['icon_image_url'])) {
        $urlToIcon ='/' . PATH_TO_SYSTEM_IMG . '/logo/icons/' . $icon['icon_image_url'];
    } else {
        $urlToIcon ='/' . PATH_TO_SYSTEM_IMG . '/logo/icons/default/favicon-96x96.png';
    }
    ?>
    <link rel="icon" href="<?php echo $urlToIcon; ?>" type="image/x-icon"/>

    <link rel="stylesheet" href="/Assets/metro-style-css/bootstrap.css"/>
<!--    <link rel="stylesheet" href="/Assets/metro-style-css/css/nonrespnsive.css">-->
    <link rel="stylesheet" href="/Assets/metro-style-css/css/m-styles.min.css"/>
    <link rel="stylesheet" href="/Assets/fa/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Assets/metro-style-css/notify-metro.css"/>
    <link rel="stylesheet" href="/Assets/metro-style-css/css/styles.css"/>
    <link rel="stylesheet" href="/Assets/metro-style-css/css/public.css"/>
</head>
<body>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1493060150997969',
            xfbml      : true,
            version    : 'v2.5'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<div class="container wrap" id="top">
    <nav class="navbar navbar-default" style="padding-bottom: 50px; background-color: transparent; border: none;">
        <div class="container">
            <div class="navbar-header" style="margin-left: 180px;">
                <a class="navbar-brand" href="index.php">
                <?php
                $logo = $links->getHEaderImage();
                $img = $logo[0]['header_image_url'];
                if ($img == false) {
                    echo "<img src='/" . PATH_TO_SYSTEM_IMG . "/logo/default/default.png'/>";
                } else {
                    echo "<img src='/" .PATH_TO_SYSTEM_IMG . "/logo/" . $img . "' />";
                }
                ?>
                </a>
            </div>
        </div>
    </nav>
